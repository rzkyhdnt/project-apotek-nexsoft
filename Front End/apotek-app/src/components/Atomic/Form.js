import React, { Component } from "react";
import APIServices from "../../APIServices/APIServices";
import swal from "sweetalert";

function handleSubmit(event) {
  event.preventDefault();
  const form = document.querySelector("form");
  const data = new FormData(form);
  APIServices.saveData("obat", data);
  swal("Data berhasil disimpan!", "Product obat ditambahkan", "success");
  window.setTimeout(function () {
    window.location.reload();
  }, 500);
}

export default function Form() {
  return (
    <div class="main">
      <div class="main-content">
        <div class="container-fluid">
          <div class="panel panel-headline">
            <div class="panel-heading">
              <h3 class="panel-title">Tambah Obat</h3>
            </div>
            <div class="panel-body">
              <form action="">
                <div class="row">
                  <div class="col-sm-2">
                    <div>
                      <h3 class="panel-title">ID</h3>
                    </div>
                    <div>
                      <input
                        id="id"
                        class="form-control"
                        name="id"
                        type="number"
                      />
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <div>
                      <h3 class="panel-title">Nama Obat</h3>
                    </div>
                    <div>
                      <input
                        id="nama-obat"
                        class="form-control"
                        name="nama"
                        type="text"
                      />
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <div>
                      <h3 class="panel-title">Jenis Obat</h3>
                    </div>
                    <div>
                      <input
                        id="jenis"
                        class="form-control"
                        name="jenis"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-2">
                    <div>
                      <h3 class="panel-title">Stok</h3>
                    </div>
                    <div>
                      <input
                        id="stok"
                        class="form-control"
                        name="stock"
                        type="number"
                      />
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <div>
                      <h3 class="panel-title">Harga</h3>
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input
                        class="form-control"
                        id="price"
                        name="price"
                        type="number"
                      />
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <div>
                      <h3 class="panel-title">Dosis</h3>
                    </div>
                    <div>
                      <input
                        id="dosis"
                        class="form-control"
                        name="dosis"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div>
                      <h3 class="panel-title">Image</h3>
                    </div>
                    <div>
                      <input
                        id="picture"
                        class="form-control"
                        name="picture"
                        type="file"
                      />
                    </div>
                  </div>
                  <div class="col-sm-7">
                    <div>
                      <h3 class="panel-title">Deskripsi</h3>
                    </div>
                    <div>
                      <textarea
                        class="form-control"
                        name="desc"
                        id="desc"
                        cols="65%"
                        rows="5"
                      ></textarea>
                    </div>
                  </div>
                  <button
                    type="submit"
                    class="btn btn-primary"
                    onClick={handleSubmit}
                  >
                    Save
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

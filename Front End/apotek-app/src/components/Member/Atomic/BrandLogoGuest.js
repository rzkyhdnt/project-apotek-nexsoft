import React, { Component } from "react";
import Logo from "../../../assets/img/Users/nexmed-logo.png";
import { connect } from "react-redux";

class BrandLogoGuest extends Component {
  render() {
    return (
      <a
        class="navbar-brand"
        href="#"
        onClick={() => this.props.setPage("Home Guest")}
      >
        <img src={Logo} width="218  " height="64" alt="" />
      </a>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(BrandLogoGuest);

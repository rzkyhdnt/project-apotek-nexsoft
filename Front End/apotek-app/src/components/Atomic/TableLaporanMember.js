import React, { Component } from "react";
import DetailsModalLaporan from "./DetailsModalLaporan";
import APIServices from "../../APIServices/APIServices";
import SearchBar from "./SearchBar";

const { search } = window.location;
const query = new URLSearchParams(search).get("s");

export default class TableLaporan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: this.props.headers,
      data: [],
      searchQuery: query,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(param) {
    this.setState({ searchQuery: param });
  }

  async componentDidMount() {
    const response = await APIServices.getData("transaction/member");
    const body = await response.data;
    console.log(body);
    this.setState({ data: body });
  }

  render() {
    const { headers, data } = this.state;
    const filterPosts = (data, query) => {
      if (!query) {
        return data;
      }

      return data.filter((post) => {
        const date = post.date.includes(query);
        return date;
      });
    };
    const filteredPost = filterPosts(data, this.state.searchQuery);

    return (
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">{this.props.title}</h3>
              </div>
              <div class="panel-body">
              <SearchBar
                  searchQuery={this.state.searchQuery}
                  setSearchQuery={this.handleChange}
                />
                <table class="table table-hover">
                  <thead>
                    <tr>
                      {headers.map((item) => (
                        <th key={item.id} scope="col">
                          {item}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {filteredPost.map((item) => (
                      <tr key={item.key}>
                        <td>{item.date}</td>
                        <td>{item.users.userName}</td>
                        <td>{"Rp  " + item.price}</td>
                        <td>{item.deliverPrice}</td>
                        <td>{"Rp  " + item.totalPrice}</td>
                        <td>
                          <DetailsModalLaporan
                            nameButton="Details"
                            id={item.id}
                            status="member"
                          />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

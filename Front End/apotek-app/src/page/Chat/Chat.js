import React, { Component } from "react";
import SockJsClient from "react-stomp";
import APIServices from "../../APIServices/APIServices";
import ImgChat from "../../assets/img/chat.png";
import Modal from "react-bootstrap/Modal";

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      oldMessage: [],
      typedMessage: "",
      data: [],
      toAdmin: [],
      name: localStorage.getItem("username"),
      showModal: false,
    };
    this.showModal = this.showModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount() {
    APIServices.getData("chat")
      .then((res) => {
        this.setState({ data: res.data });
      })
      .then(
        APIServices.getData("users/admin").then((res) => {
          this.setState({ toAdmin: res.data });
        })
      );
  }

  showModal() {
    this.setState({ showModal: true });
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  sendMessage = () => {
    let chat = {
      fromCustomer: {
        id: localStorage.getItem("id"),
      },
      date: new Date(),
      message: this.state.typedMessage,
      fromAdmin: this.state.toAdmin[0],
    };
    APIServices.saveData("chat", chat);

    this.clientRef.sendMessage(
      "/app/user-all",
      JSON.stringify({
        sender: this.state.name,
        message: this.state.typedMessage,
      })
    );
  };

  displayMessages = () => {
    return (
      <div>
        {this.state.data
          .filter((item) => item.fromCustomer.id == localStorage.getItem("id"))
          .map((item) => (
            <div className="buble-left    ">
              <p>{item.fromCustomer.userName}</p>
              <h5>{item.message}</h5>
            </div>
          ))}

        {this.state.messages.map((msg) => {
          return (
            <div>
              {
                <div className="buble">
                  <p>{msg.sender}</p>
                  <h5>{msg.message}</h5>
                </div>
              }
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    return (
      <div className="for-chat">
        <div className="chat" onClick={this.showModal}>
          <img src={ImgChat} />
        </div>
        <div className="modal-chat">
          <Modal
            show={this.state.showModal}
            onHide={this.closeModal}
            className="chat-pop"
          >
            <Modal.Header className="chat-us  ">
              <Modal.Title>Chat Us!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="align-center">
                User : <p className="title1"> {this.state.name}</p>
              </div>
              <div className="align-center">
                <br />
                <br />
                <div className="align-center">{this.displayMessages()}</div>
                <table className="type-chat">
                  <tr>
                    <td>
                      <textarea
                        id="outlined-basic"
                        label="Enter Message to Send"
                        variant="outlined"
                        onChange={(event) => {
                          this.setState({ typedMessage: event.target.value });
                        }}
                      />
                    </td>
                    <td>
                      <button className="chat-btn" onClick={this.sendMessage}>
                        Send
                      </button>
                    </td>
                  </tr>
                </table>
              </div>
              <br />
              <br />

              <SockJsClient
                url="http://localhost:8080/ws/"
                topics={["/topic/user"]}
                onConnect={() => {
                  console.log("connected");
                }}
                onDisconnect={() => {
                  console.log("Disconnected");
                }}
                onMessage={(msg) => {
                  var jobs = this.state.messages;
                  jobs.push(msg);
                  this.setState({ messages: jobs });
                }}
                ref={(client) => {
                  this.clientRef = client;
                }}
              />
            </Modal.Body>
          </Modal>
        </div>
      </div>
    );
  }
}

export default Chat;

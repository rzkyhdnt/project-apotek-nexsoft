import React, { Component } from "react";
import "../../assets/vendor/linearicons/style.css"

const SearchBar = ({searchQuery, setSearchQuery}) => (
    <form action="/" method="get">
        <input
            value = {searchQuery}
            onInput = {e => setSearchQuery(e.target.value)}
            type="text"
            id="header-search"
            placeholder="Search users"
        />
        <i className="lnr lnr-magnifier" id="search-btn"/>
    </form>
)

export default SearchBar;

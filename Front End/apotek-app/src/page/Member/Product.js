import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/customer.scss";
import Navbar from "../../components/Member/Navbar";
import HeaderInfo from "../../components/Member/HeaderInfo";
import ProductContent from "../../components/Member/ProductContent";
import Footer from "../../components/Member/Atomic/Footer";


export default class Product extends Component {
  render() {
    return (
      <>
        <Navbar />
        <HeaderInfo />
        <ProductContent />
        <Footer />
      </>
    );
  }
}

import React, { Component } from "react";
import NavbarAdmin from "../../components/NavbarAdmin";
import SidebarAdmin from "../../components/SidebarAdmin";
import TableLaporanNonMember from "../../components/Atomic/TableLaporanNonMember";

export default class LaporanNonMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: [
        "Tanggal Beli",
        "Username",
        "Harga Obat",
        "Harga Ongkir",
        "Total Harga",
        "Details",
      ],
    };
  }
  render() {
    const { headers } = this.state;
    return (
      <div id="wrapper">
        <NavbarAdmin />
        <SidebarAdmin />
        <TableLaporanNonMember title="Laporan Non Member" headers={headers} />
      </div>
    );
  }
}

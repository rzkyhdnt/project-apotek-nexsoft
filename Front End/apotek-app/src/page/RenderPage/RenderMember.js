import React, { Component } from "react";
import Home from "../Member/Home";
import CartContent from "../../components/Member/CartContent";
import Recipe from "../../components/Member/Atomic/Recipe";
import ProductContent from "../../components/Member/ProductContent";
import Navbar from "../../components/Member/Navbar";
import Footer from "../../components/Member/Atomic/Footer";
import DetailProductContent from "../../components/Member/DetailProductContent";
import MultiCarousel from "../../components/Member/Atomic/MultiCarousel";
import EditProfile from "../../components/Member/EditProfile";
import HeaderInfo from "../../components/Member/HeaderInfo";
import Chat from "../Chat/Chat";
import EditPassword from "../../components/Member/EditPassword";
import HistoryTransaction from "../../components/Member/HistoryTransaction";

export default function RenderMember(val) {
 
  switch (val) {
    case "": 
      return (
        <>
          <Home />
          <Chat />
        </>
      );
    case "Resep":
      return (
        <>
          <Home />
          <Chat />
        </>
      );
    case "Home":
      return (
        <>
          <Home />
          <Chat />
        </>
      );
    case "Cart":
      return (
        <>
          <Navbar />
          <CartContent />
          <Chat />
        </>
      );
    // case "Resep" : return <Recipe />
    case "Product":
      return (
        <>
          <Navbar />
          <ProductContent />
          <Footer />
          <Chat />
        </>
      );
    case "Product Detail":
      return (
        <>
          <Navbar />
          <DetailProductContent />
          <Chat />
        </>
      );
    case "Edit Profile":
      return (
        <>
          <Navbar />
          <HeaderInfo />
          <EditProfile />
          <Footer />
          <Chat />
        </>
      );
    case "Edit Password":
      return (
        <>
          <Navbar />
          <HeaderInfo />
          <EditPassword />
          <Footer />
          <Chat />
        </>
      );
    case "History Transaksi":
      return (
        <>
          <Navbar />
          <HeaderInfo />
          <HistoryTransaction />
          <Footer />
          <Chat />
        </>
      );
  }
}

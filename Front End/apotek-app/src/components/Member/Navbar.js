import React, { Component } from "react";
import BrandLogo from "./Atomic/BrandLogo";
import CartIcon from "./Atomic/CartIcon";
import SearchBar from "./Atomic/SearchBar";
import DropdownNavbar from "./Atomic/DropdownNavbar";
import "bootstrap/dist/css/bootstrap.min.css";

function Navbar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">
      <BrandLogo />
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <SearchBar />
        <ul className="navbar-nav ml-auto">
          <li>
            <CartIcon />
          </li>
          <li className="nav-item dropdown">
            <DropdownNavbar />
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navbar;

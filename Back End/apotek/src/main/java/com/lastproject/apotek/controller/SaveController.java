package com.lastproject.apotek.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.lastproject.apotek.model.Cart;
import com.lastproject.apotek.model.Chat;
import com.lastproject.apotek.model.Obat;
import com.lastproject.apotek.model.ObatDTO;
import com.lastproject.apotek.model.Resep;
import com.lastproject.apotek.model.ResepDTO;
import com.lastproject.apotek.model.Status;
import com.lastproject.apotek.model.Transaction;
import com.lastproject.apotek.model.Users;
import com.lastproject.apotek.repository.ObatRepo;
import com.lastproject.apotek.repository.UsersRepo;
import com.lastproject.apotek.service.ModelService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/save")
public class SaveController {
    @Autowired
    ModelService modelService;

    @Autowired
    UsersRepo usersRepo;

    @Autowired
    ObatRepo obatRepo;

    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    @PostMapping(value = "/users", consumes = "application/json")
    public void addUsers(@Valid @RequestBody Users users) {
        modelService.saveUsers(users);
    }

    @PostMapping(value = "/auth", consumes = "application/json")
    public ResponseEntity<?> auth(HttpServletRequest request,
            @RequestParam(required = false, name = "userName") String userName,
            @RequestParam(required = false, name = "password") String password) {
        try {

            Users findUser = usersRepo.findByUserName(userName);
            if (findUser != null) {
                if (encoder.matches(password, findUser.getPassword())) {
                    Map<String, Object> output = new HashMap<>();
                    output.put("id", findUser.getId());
                    output.put("username", findUser.getUserName());
                    output.put("fullname", findUser.getFullName());
                    output.put("status", findUser.getStatus());
                    System.out.println(findUser);
                    return new ResponseEntity<>(output, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>("Wrong username", HttpStatus.NOT_FOUND);
                }
            } else {
                System.out.println(findUser);
                return new ResponseEntity<>("Wrong username and password", HttpStatus.NOT_FOUND);

            }
        } catch (DataAccessException e) {
            return new ResponseEntity<>("Invalid", HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping(value = "/status", consumes = "application/json")
    public void addStatus(@RequestBody Status status) {
        modelService.saveStatus(status);
    }

    @PostMapping(value = "/transaction", consumes = "application/json")
    public void addTransaction(@RequestBody Transaction transaction) {
        modelService.saveTransaction(transaction);
    }

    @PostMapping(value = "/obats", consumes = "application/json")
    public void addObat(@RequestBody Obat obat) {
        modelService.saveObat(obat);
    }

    @PostMapping(value = "/cart", consumes = "application/json")
    public void addCart(@RequestBody Cart cart) {
        modelService.saveCart(cart);
    }

    @PostMapping(value = "/chat", consumes = "application/json")
    public void saveChat(@RequestBody Chat chat) {
        modelService.saveChat(chat);
    }

    @PostMapping(value = "/resep", consumes = "application/json")
    public void addResep(@RequestBody Resep resep) {
        modelService.saveResep(resep);
    }

    @PostMapping(value = "/reseps", consumes = { "multipart/form-data", "application/x-www-form-urlencoded",
            "application/json" })
    public void addReseps(@ModelAttribute ResepDTO resepDTO) {
        try {
            modelService.saveImageResep(resepDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping(value = "/obat", consumes = { "multipart/form-data", "application/x-www-form-urlencoded",
            "application/json" })
    public void addObat(@ModelAttribute ObatDTO obatDTO) {
        try {
            modelService.saveObatDTO(obatDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Put Mapping for minus stock
    @RequestMapping(value = "/obat/{id}", method = RequestMethod.PUT)
    Obat replacementObat(@RequestBody Obat obat, @PathVariable("id") Integer id) {
        return obatRepo.findById(id).map(obats -> {
            obats.setStock(obat.getStock());
            return obatRepo.save(obats);
        }).orElseGet(() -> {
            obat.setId(id);
            return obatRepo.save(obat);
        });
    }

    // Put Mapping for edit customers
    @PutMapping("/users/{id}")
    Users replacementUsers(@RequestBody Users users, @PathVariable("id") Integer id) {
        return usersRepo.findById(id).map(user -> {
            user.setUserName(users.getUserName());
            user.setFullName(users.getFullName());
            user.setAddress(users.getAddress());
            user.setEmail(users.getEmail());
            user.setPhoneNumber(users.getPhoneNumber());
            System.out.println(user);
            return usersRepo.save(user);
        }).orElseGet(() -> {
            users.setId(id);
            return usersRepo.save(users);
        });
    }

    // Put Mapping for change password
    @PutMapping("users/edit/password/{id}")
    Users replacementPassword(@RequestBody Users users, @PathVariable("id") Integer id) {
        return usersRepo.findById(id).map(user -> {
            user.setPassword(encoder.encode(users.getPassword()));
            return usersRepo.save(user);
        }).orElseGet(() -> {
            users.setId(id);
            return usersRepo.save(users);
        });
    }
}

import React, { Component } from "react";
import Logo from "../../../assets/img/Users/nexmed-logo.png";
import { connect } from "react-redux";
import createHistory from "history/createBrowserHistory";

class BrandLogo extends Component {
  handleClick = () => {
    this.props.setPage("Home");
    window.setTimeout(function () {
      window.location.reload();
    }, 400);
  };

  render() {
    return (
      <a class="navbar-brand" href="#" onClick={this.handleClick}>
        <img src={Logo} width="218  " height="64" alt="" />
      </a>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(BrandLogo);

import React, { Component } from "react";
import { Dropdown } from "react-bootstrap";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import APIServices from "../../../APIServices/APIServices";
import { withTranslation } from "react-i18next";

class DropdownNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tampung: [],
      cart: [],
    };
  }

  handleClick = () => {
    let jumlah = "";
    this.props.jumlahPerProduct.forEach((item) => {
      jumlah += item;
    });

    let tampung = jumlah.split("");

    console.log(tampung);

    const post = {
      jumlah: this.props.totalOrder,
      obat: this.props.cart,
      jumlahProduk: jumlah,
      user: {
        id: localStorage.getItem("id"),
      },
    };
    APIServices.saveData("cart", post);
    this.props.sendToLogout();
    this.props.removeCart();
    this.props.removeTotalOrder();
    localStorage.removeItem("username");
    localStorage.removeItem("id");

    <Redirect to={"/"} />;
  };

  render() {
    const username = localStorage.getItem("username");
    const { t } = this.props;
    return (
      <Dropdown>
        <Dropdown.Toggle variant="success" id="dropdown-basic">
          Hi, {username}
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item
            href="#"
            onClick={() => this.props.setPage("Edit Profile")}
          >
            {t("dropdown.title1")}
          </Dropdown.Item>
          <Dropdown.Item
            href="#"
            onClick={() => this.props.setPage("History Transaksi")}
          >
            {t("dropdown.title2")}
          </Dropdown.Item>
          <Dropdown.Item
            href="#"
            onClick={() => this.props.setPage("Edit Password")}
          >
            {t("dropdown.title3")}
          </Dropdown.Item>
          <hr />
          <Dropdown.Item href="#" onClick={this.handleClick}>
            {t("dropdown.title4")}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLogin: state.isLogin,
    status: state.status,
    cart: state.cart,
    totalOrder: state.totalOrder,
    jumlahPerProduct: state.jumlahPerProduct,
  };
};

const mapToProps = (dispatch) => {
  return {
    sendToLogout: () => dispatch({ type: "LOGOUT_REQUEST" }),
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
    removeCart: () => dispatch({ type: "REMOVE_ALL_CART" }),
    removeTotalOrder: () => dispatch({ type: "RESET_TOTAL_ORDER" }),
  };
};

export default connect(
  mapStateToProps,
  mapToProps
)(withTranslation()(DropdownNavbar));

import React, { Component } from "react";
import Footer from "../../components/Member/Atomic/Footer";
import CarouselHeader from "../../components/Member/CarouselHeader";
import HeaderInfo from "../../components/Member/HeaderInfo";
import HomeContent from "../../components/Member/HomeContent";
import NavbarGuest from "./NavbarGuest";
import { connect } from "react-redux";

class HomeGuest extends Component {
  render() {
    console.log(this.props.page)
    return (
      <>
        <NavbarGuest />
        <HeaderInfo />
        <CarouselHeader />
        <HomeContent />
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(HomeGuest);

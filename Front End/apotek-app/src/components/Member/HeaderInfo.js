import React, { Component } from "react";
import { useTranslation } from "react-i18next";

function HeaderInfo() {
  const {t} = useTranslation();

  return (
    <div class="header-info">
      <div class="left-text">
        <p>{t("heading.title")}</p>
        <ul>
          <li>
            <i class="fa fa-facebook-square" aria-hidden="true"></i>
          </li>
          <li>
            <i class="fa fa-twitter" aria-hidden="true"></i>
          </li>
          <li>
            <i class="fa fa-instagram" aria-hidden="true"></i>
          </li>
        </ul>
      </div>
      <div class="right-text">
        <ul>
          <li>
            <p>{t("heading.date")}</p>
          </li>
          <li>
            <i class="fa fa-envelope">
              {" "}
              <span>info@nexmed.com</span>{" "}
            </i>
          </li>
          <li>
            <i class="fa fa-whatsapp" aria-hidden="true">
              <span>082122116489</span>
            </i>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default HeaderInfo;

import React, { Component } from "react";
import BrandLogo from "./Atomic/BrandLogo";
import NavbarMenu from "./Atomic/NavbarMenu";

export default class NavbarAdmin extends Component {
  render() {
    return (
      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="container-fluid">
          <BrandLogo />
          <NavbarMenu />
        </div>
      </nav>
    );
  }
}

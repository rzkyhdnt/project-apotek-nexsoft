import React, { Component } from "react";
import APIServices from "../../APIServices/APIServices";
import { connect } from "react-redux";
import swal from "sweetalert";

class DetailsProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      cart: [],
      stock: 0,
    };
    this.addToCart = this.addToCart.bind(this);
    this.minusStock = this.minusStock.bind(this);
    this.addStock = this.addStock.bind(this);
  }

  componentDidMount() {
    APIServices.detailData("obat", this.props.id).then((res) => {
      const data = res.data;
      this.setState({ data: data, stock: data.stock });
    });
  }

  register() {
    this.props.removeCart();
    window.location.assign("/login");
  }

  minusStock = () => {
    this.props.addCart();
    this.setState({ stock: this.state.stock - 1 });
  };

  addStock = () => {
    const { data, stock } = this.state;
    this.props.minusCart();
    if (stock !== 0) {
      this.setState({ stock: this.state.stock + 1 });
      if (data.stock == stock) {
        this.setState({ stock: data.stock });
      }
    } else {
      this.setState({ stock: this.state.stock + 1 });
    }
  };

  showStock = () => {
    const { stock } = this.state;

    if (stock > 0) {
      return (
        <>
          <h5 class="stok">Stok tersedia : {stock}</h5>
          <button className="add-minus" onClick={() => this.addStock()}>
            -
          </button>
          <input
            type="number"
            class="quantity"
            value={this.props.orderPerProduct}
            onChange={this.handleChange}
            name="jumlah"
          />
          <button className="add-minus" onClick={() => this.minusStock()}>
            +
          </button>
        </>
      );
    } else {
      return (
        <>
          <h5 class="stok">Stok tersedia : Out of stock</h5>
          <button className="add-minus" onClick={() => this.addStock()}>
            -
          </button>
          <input
            type="number"
            class="quantity"
            value={this.props.orderPerProduct}
            onChange={this.handleChange}
            name="jumlah"
          />
          <button className="add-minus" disabled>
            +
          </button>
        </>
      );
    }
  };

  handleChange = (e) => {
    this.props.handleChange(e.target.value);
  };

  addToCart(e) {
    const totalHarga =
      parseInt(this.state.data.price) * parseInt(this.props.orderPerProduct);
    const product = {
      id: this.state.data.id,
      nama: this.state.data.nama,
      picture: this.state.data.picture,
      price: this.state.data.price,
      stock: this.state.data.stock,
      jumlah: this.props.orderPerProduct,
      totalHarga: totalHarga,
    };

    this.setState({ cart: product }, () => {
      console.log(this.state.cart);
      if (!this.props.orderPerProduct < 1) {
        this.props.wishList(this.state.cart);
        this.props.addOrderPerProduct(this.props.orderPerProduct);
      }
    });
    swal("Produk berhasil ditambah ke keranjang", " ", "success");
  }

  render() {
    const { data } = this.state;
    return (
      <div class="main-content">
        <div class="title mt-5 ml-5">
          <h4>Detail Product</h4>
        </div>
        <div class="row">
          <div class="col-sm-5">
            <div class="cart">
              <div class="card mt-3">
                <img
                  class="card-img-top"
                  src={data.picture}
                  alt="Card image cap"
                />
                <div class="card-body">
                  <h5 class="card-text">{data.nama}</h5>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <h3>{data.nama}</h3>
            <h3 class="prices">Rp. {data.price} / pcs</h3>
            {this.showStock()}
            <button class="buy" onClick={() => this.register()}>
              <i class="fa fa-shopping-cart">
                <span>ADD TO CART</span>
              </i>
            </button>
            <button
              className="see-other-product"
              onClick={() => this.props.setPage("Product")}
            >
              Lihat Produk Lain
            </button>
            <button
              className="see-cart"
              onClick={() => this.props.setPage("Cart")}
            >
              Lihat Keranjang
            </button>
          </div>

          <div class="col-sm-4">
            <div class="card text-center details">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">
                      Details
                    </a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <h5 class="card-title text-left">Jenis</h5>
                <p class="card-text text-left">{data.jenis}</p>
                <h5 class="card-title text-left mt-5">Deskripsi Product</h5>
                <p class="card-text text-left">{data.desc}</p>
                <h5 class="card-title text-left mt-5">Dosis</h5>
                <p class="card-text text-left">{data.dosis}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    id: state.id,
    cart: state.cart,
    totalPrice: state.totalPrice,
    orderPerProduct: state.orderPerProduct,
    page: state.page,
    allCart: state.allCart,
    jumlahPerProduct: state.jumlahPerProduct,
  };
};

const mapDispatch = (dispatch) => {
  return {
    addCart: () => dispatch({ type: "ADD_CART" }),
    minusCart: () => dispatch({ type: "MINUS_CART" }),
    wishList: (val) => dispatch({ type: "ADD_TO_CART", value: val }),
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
    handleChange: (val) => dispatch({ type: "HANDLE_CHANGE", value: val }),
    addOrderPerProduct: (val) =>
      dispatch({ type: "GET_ORDER_PRODUCT", value: val }),
    removeCart: () => dispatch({ type: "REMOVE_TOTAL_ORDER" }),
  };
};

export default connect(mapStateToProps, mapDispatch)(DetailsProduct);

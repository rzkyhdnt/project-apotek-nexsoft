import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import APIServices from "../../APIServices/APIServices";

export default class DetailsModalLaporan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      id: this.props.id,
      data: [],
      userName: [],
      hargaObat: [],
      namaObat: [],
      obat: [],
      status: this.props.status,
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  async componentDidMount() {
    const response = await APIServices.detailData(
      "transaction/" + this.state.status,
      this.state.id
    );
    const body = await response.data;
    this.setState({
      data: body,
      userName: body.users.userName,
      obat: body.obat,
    });
    console.log(this.state.obat);
  }

  openModal() {
    this.setState({ showModal: true });
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { data, userName, hargaObat, obat } = this.state;
    return (
      <>
        <div>
          <button className="btn btn-warning" onClick={this.openModal}>
            {this.props.nameButton}
          </button>
        </div>
        <Modal
          show={this.state.showModal}
          onHide={this.closeModal}
          animation={true}
          className="modal-content-laporan"
        >
          <Modal.Header>
            <Modal.Title>Details Laporan</Modal.Title>
          </Modal.Header>
          <Modal.Body className="modal-laporan">
            <div className="row">
              <div className="col">
                <h5>Tanggal Beli</h5>
                <input value={data.date} disabled />
              </div>
              <div className="col">
                <h5>Username</h5>
                <input value={userName} disabled />
              </div>
            </div>

            <div className="row">
              <div className="col">
                <h5>Harga obat</h5>
                <input value={"Rp " + hargaObat} disabled />
              </div>
              <div className="col">
                <h5>Jumlah</h5>
                <input value={data.quantity} disabled />
              </div>
            </div>

            <div className="row">
              <div className="col">
                <h5>List Obat</h5>
                {obat.map((item) => (
                  <li className="list-obat">
                    {item.nama + "  Rp " + item.price}
                  </li>
                ))}
              </div>
              <div className="col">
                <h5>Ongkos Kirim</h5>
                <input value={data.deliverPrice} disabled />
                <h5>Total Harga</h5>
                <input value={ "Rp   " + data.totalPrice} disabled />
                <button className="btn btn-info" onClick={this.closeModal}>
                  Close
                </button>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

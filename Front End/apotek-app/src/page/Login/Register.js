import React, { Component } from "react";
import moment from "moment";
import {
  Row,
  Col,
  Card,
  Form,
  InputGroup,
  FormControl,
  Button,
} from "react-bootstrap";
import "../../assets/vendor/font-awesome/css/font-awesome.css";
import "../../assets/css/loginregister.scss";
import { useFormik } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";

const dateNow = new Date();
const dateDetails = moment(dateNow).format("DD-MM-YYYY hh:mm");

export default function Register() {
  const formik = useFormik({
    initialValues: {
      userName: "",
      fullName: "",
      phoneNumber: "",
      address: "",
      email: "",
      password: "",
      repassword: "",
    },
    validationSchema: Yup.object({
      userName: Yup.string()
        .min(4, "Minimal 4 karakter")
        .max(15, "Maksimal 15 karakter")
        .required("Masukkan username!"),
      fullName: Yup.string()
        .min(4, "Minimal 4 karakter")
        .max(25, "Maksimal 25 karakter")
        .required("Masukkan nama lengkap!"),
      phoneNumber: Yup.number()
        .min(11, "Minimal 11 angka")
        .required("Masukkan nomor HP!"),
      address: Yup.string().required("Masukkan alamat!"),
      email: Yup.string()
        .email("Email tidak sesuai!")
        .required("Masukkan email!"),
      password: Yup.string()
        .min(6, "Minimal 6 karakter")
        .matches(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/,
          "Minimal 1 Hurf besar, 1 huruf kecil, 1 angka, 6 karakter"
        )
        .required("Masukkan password!"),
      repassword: Yup.string()
        .oneOf([Yup.ref("password")], "Password tidak sama!")
        .required("Masukkan kembali password"),
    }),
    onSubmit: () => {
      let post = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify({
          userName: document.getElementById("userName").value,
          fullName: document.getElementById("fullName").value,
          password: document.getElementById("password").value,
          address: document.getElementById("address").value,
          email: document.getElementById("email").value,
          phoneNumber: document.getElementById("phoneNumber").value,
          dateRegist: dateDetails,
          status: {
            id: 2,
          },
        }),
      };
      let send = fetch("http://localhost:8080/save/users", post);
      swal("Data berhasil disimpan!", "Data admin ditambahkan", "success");
      window.setTimeout(function () {
        window.location.reload();
      }, 1000);
    },
  });

  return (
    <div id="register">
      <Row className="justify-content-md-center">
        <Col xs={4}>
          <Card>
            <Card.Header>Register</Card.Header>
            <Card.Body>
              <form onSubmit={formik.handleSubmit}>
                <Form.Row>
                  <Form.Group>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <i className="fa fa-user" />{" "}
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        required
                        autoComplete="off"
                        type="text"
                        id="userName"
                        name="userName"
                        placeholder="Enter Your Username"
                        onChange={formik.handleChange}
                        value={formik.values.userName}
                      />
                    </InputGroup>
                    {formik.errors.userName && formik.touched.userName && (
                      <p>{formik.errors.userName}</p>
                    )}
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <i className="fa fa-address-card" />{" "}
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        required
                        autoComplete="off"
                        type="text"
                        id="fullName"
                        placeholder="Enter Your Fullname"
                        onChange={formik.handleChange}
                        value={formik.values.fullName}
                      />
                    </InputGroup>
                    {formik.errors.fullName && formik.touched.fullName && (
                      <p>{formik.errors.fullName}</p>
                    )}
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <i className="fa fa-phone" />{" "}
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        required
                        autoComplete="off"
                        type="number"
                        id="phoneNumber"
                        placeholder="Enter Your Phone Number"
                        onChange={formik.handleChange}
                        value={formik.values.phoneNumber}
                      />
                    </InputGroup>
                    {formik.errors.phoneNumber &&
                      formik.touched.phoneNumber && (
                        <p>{formik.errors.phoneNumber}</p>
                      )}
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <i className="fa fa-envelope-square" />{" "}
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        required
                        autoComplete="off"
                        type="text"
                        id="email"
                        placeholder="Enter Your Email"
                        onChange={formik.handleChange}
                        value={formik.values.email}
                      />
                    </InputGroup>
                    {formik.errors.email && formik.touched.email && (
                      <p>{formik.errors.email}</p>
                    )}
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <i className="fa fa-map" />{" "}
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        required
                        autoComplete="off"
                        type="text"
                        id="address"
                        placeholder="Enter Your Address"
                        onChange={formik.handleChange}
                        value={formik.values.address}
                      />
                    </InputGroup>
                    {formik.errors.address && formik.touched.address && (
                      <p>{formik.errors.address}</p>
                    )}
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <i className="fa fa-key" />
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        required
                        autoComplete="off"
                        type="password"
                        name="password"
                        id="password"
                        placeholder="Enter Your Password"
                        onChange={formik.handleChange}
                        value={formik.values.password}
                      />
                    </InputGroup>
                    {formik.errors.password && formik.touched.password && (
                      <p id="password-err">{formik.errors.password}</p>
                    )}
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text></InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        required
                        autoComplete="off"
                        type="password"
                        id="repassword"
                        placeholder="Re-type Your Password"
                        onChange={formik.handleChange}
                        value={formik.values.repassword}
                      />
                    </InputGroup>
                    {formik.errors.repassword && formik.touched.repassword && (
                      <p id="repassword-err">{formik.errors.repassword}</p>
                    )}
                  </Form.Group>
                </Form.Row>
                <a href="/login">Login</a>
                <Button size="sm" type="submit">
                  Register
                </Button>
              </form>
            </Card.Body>
            <Card.Footer></Card.Footer>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

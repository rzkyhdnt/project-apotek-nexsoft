import React, { Component } from 'react'
import NavbarAdmin from "../../components/NavbarAdmin";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/main.scss";
import SidebarAdmin from "../../components/SidebarAdmin";
import TableResep from "../../components/Atomic/TableResep";

export default class Resep extends Component {
    constructor(props){
        super(props);
        this.state = {
            headers : ["Date", "Username", "Fullname", "Status", "Resep"]
        }
    }
    render() {
        const {headers} = this.state;
        return (
            <div id="wrapper">
                <NavbarAdmin />
                <SidebarAdmin />
                <TableResep title="Daftar Resep" headers={headers} />
            </div>
        )
    }
}

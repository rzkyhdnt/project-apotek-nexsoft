import React, { Component } from 'react'
import SidebarMenu from './Atomic/SidebarMenu'

export default class SidebarAdmin extends Component {
    render() {
        return (
            <div id="sidebar-nav" class="sidebar">
                <SidebarMenu />
            </div>
        )
    }
}

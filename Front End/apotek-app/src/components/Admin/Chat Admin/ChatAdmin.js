import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Tab, Row, Col, Nav, Card } from "react-bootstrap";
import APIServices from "../../../APIServices/APIServices";
import SockJsClient from "react-stomp";

export default class ChatAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      oldMessage: [],
      typedMessage: "",
      data: [],
      toMember: [],
      name: localStorage.getItem("username"),
      showModal: false,
    };
  }

  componentDidMount() {
    APIServices.getData("chat")
      .then((res) => {
        this.setState({ data: res.data });
      })
      .then(
        APIServices.getData("users/member").then((resp) => {
          this.setState({ toMember: resp.data });
        })
      );
  }

  sendMessage = (val) => {
    let chat = {
      fromCustomer: {
        id: val,
      },
      date: new Date(),
      message: this.state.typedMessage,
      fromAdmin: {
          id : localStorage.getItem("id")
      },
    };
    APIServices.saveData("chat", chat);

    this.clientRef.sendMessage(
      "/app/user-all",
      JSON.stringify({
        sender: this.state.name,
        message: this.state.typedMessage,
      })
    );
    
  };

  displayMessage = () => {
    const length = this.state.toMember.length;
    console.log(this.state.typedMessage);
    return (
      <div>
        <Tab.Container id="left-tabs-example">
          {this.state.toMember.map((item, index) => (
            <Col sm={3} className="room">
              <Nav variant="pills" className="flex-column">
                <Nav.Item>
                  <Nav.Link key={index} eventKey={index}>
                    {item.userName}
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
          ))}

          <Col sm={8} className="chat-message">
            <Tab.Content>
              {this.state.toMember.map((data, index) => (
                <>
                  <Tab.Pane eventKey={index}>
                    <div className="chat-title">
                      <h4>{data.fullName}</h4>
                      <h5>{data.status.name}</h5>
                    </div>
                    <Card>
                      {this.state.data
                        .filter(
                          (name) => name.fromCustomer.userName === data.userName
                        )
                        .map((item) => (
                          <>
                            <Card.Body>
                              {item.message}
                            </Card.Body>
                          </>
                        ))}
                      {this.state.messages.map((msg) => {
                        return (
                          <div>
                            {
                              <div className="buble">
                                <p>{msg.sender}</p>
                                <h5>{msg.message}</h5>
                              </div>
                            }
                          </div>
                        );
                      })}
                    </Card>
                    <div className="reply-chat">
                      <input
                        onChange={(event) => {
                          this.setState({ typedMessage: event.target.value });
                        }}
                      />
                      <button onClick={() => this.sendMessage(data.id)}>SEND</button>
                    </div>
                  </Tab.Pane>
                </>
              ))}
            </Tab.Content>
          </Col>
        </Tab.Container>
      </div>
    );
  };

  render() {
    const { data } = this.state;
    console.log(this.state.data[this.state.data.length - 1]);
    return (
      <div class="main">
        <div class="main-content">
          <div class="container-fluid container-chat">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">Room Chat</h3>
              </div>
              <div class="panel-body">{this.displayMessage()}</div>
            </div>
          </div>
        </div>
        <SockJsClient
          url="http://localhost:8080/ws/"
          topics={["/topic/user"]}
          onConnect={() => {
            console.log("connected");
          }}
          onDisconnect={() => {
            console.log("Disconnected");
          }}
          onMessage={(msg) => {
            var jobs = this.state.messages;
            jobs.push(msg);
            this.setState({ messages: jobs });
          }}
          ref={(client) => {
            this.clientRef = client;
          }}
        />
      </div>
    );
  }
}

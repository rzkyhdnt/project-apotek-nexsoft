import React, { Component } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Card, Button } from "react-bootstrap";
import APIServices from "../../../APIServices/APIServices";
import { connect } from "react-redux";

const jumlah = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5,
    slidesToSlide: 1,
  },
};

class MultiCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  async componentDidMount() {
    const response = await APIServices.getData("obat");
    this.setState({ data: response.data });
  }

  // formatNumber(num) {
  //   return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  // }

  handleClick(e) {
    this.props.setPage("Product Detail");
    this.props.handleClick(e);
    window.setTimeout(function () {
      window.location.reload();
    }, 500);
  }

  render() {
    const { data } = this.state;
    const { search } = this.props;
    const filterPosts = (data, search) => {
      if (!search) {
        return data;
      }
      return data.filter((post) => {
        const productName = post.nama.toLowerCase().includes(search);
        return productName;
      });
    };
    const filteredPost = filterPosts(data, search);
    console.log(filteredPost);

    return (
      <>
        <Carousel
          swipeable={false}
          draggable={false}
          responsive={jumlah}
          infinite={true}
          autoPlaySpeed={1000}
          keyBoardControl={true}
          customTransition="all .5"
          transitionDuration={500}
        >
          {filteredPost.map((item) => (
            <Card style={{ width: "18rem" }} key={item.id}>
              <a href="#" onClick={() => this.handleClick(item.id)}>
                <Card.Img variant="top" src={item.picture} />
                <Card.Body>
                  <Card.Title>{item.nama}</Card.Title>
                  <h3>Rp {item.price}</h3>
                </Card.Body>
              </a>
            </Card>
          ))}
        </Carousel>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.id,
    page: state.page,
    search : state.search
  };
};

const MapDispatch = (dispatch) => {
  return {
    handleClick: (val) => dispatch({ type: "GET_ID", value: val }),
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, MapDispatch)(MultiCarousel);

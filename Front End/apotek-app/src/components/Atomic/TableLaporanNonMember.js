import React, { Component } from "react";
import APIService from "../../APIServices/APIServices";
import DetailsModalLaporan from "./DetailsModalLaporan";

export default class TableLaporan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: this.props.headers,
      data: [],
    };
  }

  async componentDidMount() {
    const response = await APIService.getData("transaction/nonmember");
    const body = await response.data;
    console.log(body);
    this.setState({ data: body });
  }

  render() {
    const { headers, data } = this.state;
    return (
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">{this.props.title}</h3>
              </div>
              <div class="panel-body">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      {headers.map((item) => (
                        <th key={item.id} scope="col">
                          {item}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((item) => (
                      <tr key={item.key}>
                        <td>{item.date}</td>
                        <td>{item.users.userName}</td>
                        <td>{"Rp  " + item.price}</td>
                        <td>{item.deliverPrice}</td>
                        <td>{item.totalPrice}</td>
                        <td>
                          <DetailsModalLaporan nameButton="Details" id={item.id} status="nonmember" />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

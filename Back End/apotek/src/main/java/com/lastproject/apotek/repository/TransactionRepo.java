package com.lastproject.apotek.repository;

import java.util.List;

import com.lastproject.apotek.model.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionRepo extends JpaRepository<Transaction, Integer> {
    @Query(nativeQuery = true, value = "select * from transaction left join resep on transaction.resep_id = resep.id join users on transaction.users_id = users.id where (resep_id is null or resep.status = 'Disetujui') and status_id=2;")
    public List<Transaction> findMemberTransaction();

    @Query(nativeQuery = true, value = "select * from transaction left join resep on transaction.resep_id = resep.id join users on transaction.users_id = users.id where (resep_id is null or resep.status = 'Disetujui') and status_id=3;")
    public List<Transaction> findGuestTransaction();

    @Query(nativeQuery = true, value = "select * from transaction where resep_id is not null")
    public List<Transaction> findAllTransaction();

    @Query(nativeQuery = true, value = "select * from transaction join resep on transaction.resep_id = resep.id join users on transaction.users_id = users.id where resep.status='Menunggu' and (users.status_id=3 or users.status_id=2)")
    public List<Transaction> findResepToAgree();

    @Query(nativeQuery = true, value = "select sum(total_price) from transaction")
    public Long getProfit();
}

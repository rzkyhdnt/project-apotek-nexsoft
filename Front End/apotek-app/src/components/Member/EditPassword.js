import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { withTranslation } from "react-i18next";
import swal from "sweetalert";
import { connect } from "react-redux";

class EditPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      errorMessage: "",
      errorConfirm: "",
      password: "",
      confirmPassword: "",
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  send() {
    fetch(
      `http://localhost:8080/save/auth?userName=${localStorage.getItem(
        "username"
      )}&password=${this.state.oldPassword}`,
      {
        method: "POST",
        headers: { "Content-type": "application/json; charset=UTF-8" },
      }
    ).then((resp) => {
      if (!resp.ok) {
        this.setState({ errorMessage: "Password lama tidak sesuai!" });
      }
    });

    if (this.state.password !== this.state.confirmPassword) {
      this.setState({ errorConfirm: "Password tidak sama!" });
    } else if (
      this.state.password === "" ||
      this.state.confirmPassword === ""
    ) {
      this.setState({ errorConfirm: "Password tidak boleh kosong" });
    } else {
      let post = {
        method: "PUT",
        headers: { "Content-Type": "application/json; charset=UTF-8" },
        body: JSON.stringify({
          id: localStorage.getItem("id"),
          password: this.state.password,
        }),
      };
      fetch(
        "http://localhost:8080/save/users/edit/password/" +
          localStorage.getItem("id"),
        post
      );
      swal("Password berhasil di edit", "", "success");
      this.props.setPage("Home");
    }
  }

  render() {
    const { data } = this.state;
    const { t } = this.props;
    return (
      <div className="main-content">
        <div className="side-content">
          <div class="row">
            <a href="#" onClick={() => this.props.setPage("Edit Profile")}>
              {t("user.side.profil")}
            </a>
          </div>
          <div class="row">
            <a href="#" onClick={() => this.props.setPage("History Transaksi")}>
              {t("user.side.history")}
            </a>
          </div>
          <div class="row change-password">
            <a href="#" onClick={() => this.props.setPage("Edit Password")}>
              {t("user.side.password")}
            </a>
          </div>
        </div>
        <div className="divider-vertical"></div>
        <div className="form-content">
          <h4>Ubah Password</h4>
          <div className="row">
            <div className="col-10">
              <h5 hidden>ID</h5>
            </div>
            <div className="col-2">
              <input type="text" id="id" hidden />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <h5>{t("user.main.password.oldpassword")}</h5>
            </div>
            <div className="col-8">
              <input
                name="oldPassword"
                id="oldPassword"
                onChange={this.handleChange}
                type="password"
              />
              <span>{this.state.errorMessage}</span>
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <h5>{t("user.main.password.newpassword")}</h5>
            </div>
            <div className="col-8">
              <input
                type="password"
                id="newPassword"
                name="password"
                onChange={this.handleChange}
              />
              <span>{this.state.errorConfirm}</span>
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <h5>{t("user.main.password.confirm")}</h5>
            </div>
            <div className="col-8">
              <input
                type="password"
                id="confirmPassword"
                name="confirmPassword"
                onChange={this.handleChange}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <button
                className="btn btn-primary"
                type="submit"
                onClick={() => this.send("Edit Password")}
              >
                {t("user.main.profil.button")}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatch
)(withTranslation()(EditPassword));

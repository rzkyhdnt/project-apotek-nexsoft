import React, { Component } from "react";
import MultiCarousel from "./Atomic/MultiCarousel";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

class HomeContent extends Component {
  render() {
    const { t } = this.props;
    return (
      <div class="main-content">
        <div class="title mt-5 ml-5">
          <h4>{t("home.title1")}</h4>
          <a
            href="#"
            class="btn btn-primary mr-5"
            onClick={() => this.props.setPage("Product")}
          >
            {t("home.title2")}
          </a>
          <div class="dropdown-divider mt-4 mb-4"></div>
        </div>
        <MultiCarousel />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatch
)(withTranslation()(HomeContent));

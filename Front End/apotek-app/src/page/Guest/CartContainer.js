import React, { Component } from "react";
import "../../assets/vendor/linearicons/style.css";
import MultiCarousel from "../../components/Member/Atomic/MultiCarousel";
import { connect } from "react-redux";
import moment from "moment";
import swal from "sweetalert";
import APIServices from "../../APIServices/APIServices";
import { withTranslation } from "react-i18next";

const dateNow = new Date();
const dateDetails = moment(dateNow).format("DD-MM-YYYY hh:mm");

class CartContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      totalPrice: 0,
      totalPriceProduct: 0,
      ongkir: 15000,
      date: null,
      resepImage: null,
      resepText: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }

  deleteCart(id, jumlah) {
    this.props.deleteCart(id);
    this.props.deleteOrder(jumlah);
    this.props.removeQty(jumlah);
    window.setTimeout(function () {
      window.location.reload();
    }, 300);
  }

  sendResep() {
    let post = {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify({
        quantity: this.props.totalOrder,
        date: dateDetails,
        price: this.props.totalPrice,
        deliverPrice: 0,
        totalPrice: this.props.totalPrice,
        users: {
          id: localStorage.getItem("id"),
        },
        resep: {
          date: dateDetails,
          resepImage: this.state.resepImage,
        },
        obat: this.props.cart,
      }),
    };
    const form = document.querySelector(".form-resep");
    const data = new FormData(form);
    APIServices.saveData("reseps", data);
    let send = fetch("http://localhost:8080/save/transaction", post);
    swal(
      "Berhasil mengirim reseop",
      "Tunggu hingga resep disetujui oleh admin",
      "success"
    );
  }

  send() {
    let post = {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify({
        quantity: this.props.totalOrder,
        date: dateDetails,
        price: this.props.totalPrice,
        deliverPrice: 0,
        totalPrice: this.props.totalPrice,
        users: {
          id: localStorage.getItem("id"),
        },
        obat: this.props.cart,
      }),
    };

    let tempStock = [];
    this.props.cart.forEach((val) =>
      tempStock.push({ id: val.id, stock: val.stock - val.jumlah })
    );

    tempStock.map((item) => {
      let put = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify({
          id: item.id,
          stock: item.stock,
        }),
      };
      let sends = fetch("http://localhost:8080/save/obat/" + item.id, put);
    });

    let send = fetch("http://localhost:8080/save/transaction", post);
    this.props.removeTotalOrder(0);
    this.props.removeCart();
    swal("Berhasil membeli obat yang telah dipilih!", "", "success");
  }

  componentDidMount() {
    let totalBarang = 0;
    const ongkir = 150000;
    let jumlah = "";
    this.props.jumlahPerProduct.forEach((item) => {
      jumlah += item;
    });
    let temp = jumlah.split("");

    let findTotalHarga = this.props.cart.map((item, index) => {
      totalBarang += parseInt(item.price * temp[index]);
    });
    this.setState({ totalPrice: totalBarang }, () => {
      this.props.getTotalPrice(this.state.totalPrice);
    });
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  register() {
    window.location.assign("/login");
  }

  getOngkirTotalPrice(val) {
    const { t } = this.props;
    if (val > 100000) {
      return (
        <>
          <div className="row">
            <div className="col judul">
              <p>{t("cartContainer.invoice.deliverPrice")}</p>
            </div>
            <div className="col value">
              <p>{t("cartContainer.invoice.ongkir")}</p>
            </div>
          </div>
          <hr />
          <div className="row total">
            <div className="col judul total-harga">
              <p>{t("cartContainer.invoice.totalPrice")}</p>
            </div>
            <div className="col value total-harga">
              <p>Rp. {this.formatNumber(this.state.totalPrice)}</p>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="row">
            <div className="col judul">
              <p>{t("cartContainer.invoice.deliverPrice")}</p>
            </div>
            <div className="col value">
              <p>Rp. 15,000</p>
            </div>
          </div>
          <hr />
          <div className="row total">
            <div className="col judul total-harga">
              <p>{t("cartContainer.invoice.totalPrice")}</p>
            </div>
            <div className="col value total-harga">
              <p>
                Rp.{" "}
                {this.formatNumber(this.state.totalPrice + this.state.ongkir)}
              </p>
            </div>
          </div>
        </>
      );
    }
  }

  checkResep() {
    const { t } = this.props;
    if (this.state.resepImage !== null) {
      return (
        <div className="card">
          <div className="card-body">
            <h5>{t("cartContainer.invoice.resep.title1")}</h5>
            <p>{t("cartContainer.invoice.resep.title2")}</p>
            <button className="btn btn-primary" onClick={this.register}>
              {t("cartContainer.invoice.resep.button")}
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="card">
          <div className="card-body">
            <h5>
              {t("cartContainer.invoice.title1")} <hr />
            </h5>
            <div className="row">
              <div className="col judul">
                <p>{t("cartContainer.invoice.totalItem")}</p>
              </div>
              <div className="col value">
                <p>({this.props.totalOrder})</p>
              </div>
            </div>
            <div className="row">
              <div className="col judul">
                <p>{t("cartContainer.invoice.itemPrice")}</p>
              </div>
              <div className="col value">
                <p>Rp. {this.formatNumber(this.state.totalPrice)}</p>
              </div>
            </div>
            {this.getOngkirTotalPrice(this.state.totalPrice)}
            <button className="btn btn-primary" onClick={this.register}>
              {t("cartContainer.invoice.button")}
            </button>
          </div>
        </div>
      );
    }
  }

  displayCart() {
    const { t } = this.props;
    if (
      this.state.data.resep === undefined ||
      this.state.data.resep.status === "Disetujui"
    ) {
      return (
        <div className="main-content">
          <div className="row cart-content">
            <div className="col-7">
              <div className="select-all">
                <input type="checkbox" /> <p>{t("cartContainer.title1")}</p>
              </div>
              <div className="promo">
                <p>{t("cartContainer.sub1")}</p>
                <p>{t("cartContainer.sub2")}</p>
              </div>
              <hr />
              {this.props.cart.map((item, index) => (
                <div className="select-all" key={item.key}>
                  <input type="checkbox" />
                  <img src={item.picture} />
                  <div className="produk">
                    <h5>{item.nama}</h5>
                    <h5 className="price">
                      Rp. {this.formatNumber(item.price)}
                    </h5>
                    <p className="stok">
                      {t("cartContainer.stock")} {item.stock}
                    </p>
                    <div className="jumlah">
                      <input
                        type="number"
                        id="stock"
                        value={item.stock}
                        hidden
                      />
                      <input
                        type="number"
                        name="id"
                        id="id"
                        value={item.id}
                        hidden
                      />
                      <input
                        type="number"
                        class="quantity"
                        value={this.props.jumlahPerProduct[index]}
                        name="jumlah"
                        id="jumlah"
                        disabled
                      />

                      <button
                        className="delete"
                        onClick={() =>
                          this.deleteCart(
                            item.id,
                            this.props.jumlahPerProduct[index]
                          )
                        }
                      >
                        <i className="lnr lnr-trash" />
                      </button>
                    </div>
                  </div>
                  <hr />
                </div>
              ))}

              <div className="resep">
                <p>{t("cartContainer.recipe")} :</p>
                <form className="form-resep">
                  <input
                    type="file"
                    name="resepImage"
                    onChange={this.handleChange}
                  />
                  <input type="text" name="date" value={dateDetails} hidden />
                </form>
              </div>
            </div>
            <div className="col-5">
              <div className="invoice">{this.checkResep()}</div>
            </div>
          </div>
          <div className="list-product">
            <h5>{t("home.title2")}</h5>
            <MultiCarousel />
          </div>
        </div>
      );
    }
  }

  render() {

    return <>{this.displayCart()}</>;
  }
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    id: state.id,
    cart: state.cart,
    totalPrice: state.totalPrice,
    orderPerProduct: state.orderPerProduct,
    allCart: state.allCart,
    jumlahPerProduct: state.jumlahPerProduct,
  };
};

const mapDispatch = (dispatch) => {
  return {
    addCart: () => dispatch({ type: "ADD_CART" }),
    minusCart: () => dispatch({ type: "MINUS_CART" }),
    handleChange: (e) => dispatch({ type: "HANDLE_CHANGE", value: e }),
    getTotalPrice: (e) => dispatch({ type: "GET_TOTAL_PRICE", value: e }),
    deleteCart: (e) => dispatch({ type: "DELETE_ITEM", value: e }),
    deleteOrder: (e) => dispatch({ type: "DELETE_ORDER", value: e }),
    removeTotalOrder: (e) => dispatch({ type: "REMOVE_TOTAL_ORDER", value: e }),
    removeCart: () => dispatch({ type: "REMOVE_ALL_CART" }),
    removeQty: (e) => dispatch({ type: "DELETE_QTY_PRODUCT", value: e }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatch
)(withTranslation()(CartContainer));

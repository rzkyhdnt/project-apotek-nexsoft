import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/main.scss";
import Table from "../../components/Atomic/TableObat";
import NavbarAdmin from "../../components/NavbarAdmin";
import SidebarAdmin from "../../components/SidebarAdmin";

export default class DataObat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: [
        "Gambar",
        "Nama",
        "Jenis",
        "Stok",
        "Harga",
        "Dosis",
        "Keterangan",
        "Action",
      ],
      data: [],
    };
  }

  render() {
    const { headers, data } = this.state;
    return (
      <div id="wrapper">
        <NavbarAdmin />
        <SidebarAdmin />
        <Table headers={headers} title="Daftar Obat" data={data} id={data.id} />
      </div>
    );
  }
}

package com.lastproject.apotek.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    private int id;
    @NotNull
    @NotBlank
    @Size(min = 2, max = 15)
    @Column(unique = true)
    private String userName;
    @NotNull
    @NotBlank
    @Size(min = 4, max = 25)
    private String fullName;
    @NotNull
    @NotBlank
    @Size(min = 6)
    private String password;
    @NotNull
    @NotBlank
    private String address;
    @NotBlank
    @Email
    private String email;
    @NotBlank
    @NotNull
    @Size(min = 11)
    private String phoneNumber;
    private String dateRegist;

    @OneToOne
    private Status status;

    private boolean deleted = false;

}

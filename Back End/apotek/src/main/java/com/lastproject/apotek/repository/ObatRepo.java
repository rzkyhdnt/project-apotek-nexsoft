package com.lastproject.apotek.repository;

import java.util.ArrayList;

import com.lastproject.apotek.model.Obat;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

public interface ObatRepo extends JpaRepository<Obat, Integer> {
    @Query(nativeQuery = true, value = "select * from obat where deleted = false")
    ArrayList<Obat> findAllObat();

    @Modifying(clearAutomatically = true)
    @Query(nativeQuery = true, value = "update obat set deleted=true where obat.id= :id")
    void deleteObat(@Param("id") Integer id);
}

import React, { Component } from "react";
import APIService from "../../APIServices/APIServices";
import DetailsModalResep from "./DetailsModalResep";

export default class TableResep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: this.props.headers,
      data: [],
    };
  }

  async componentDidMount() {
    const response = await APIService.getData("resep/list");
    const body = await response.data;
    this.setState({ data: body });
  }

  render() {
    const { headers, data } = this.state;
    console.log(this.state.data);
    return (
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">{this.props.title}</h3>
              </div>
              <div class="panel-body">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      {headers.map((item) => (
                        <th key={item.id} scope="col">
                          {item}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((item) => (
                      <tr key={item.id}>
                        <td>{item.date}</td>
                        <td>{item.users.userName}</td>
                        <td>{item.users.fullName}</td>
                        <td>{item.users.status.name}</td>
                        <td>
                          <DetailsModalResep
                            nameButton={"Details"}
                            id={item.resep.id}
                          />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

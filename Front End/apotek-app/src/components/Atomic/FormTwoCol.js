import React, { Component } from "react";
import moment from "moment";
import swal from "sweetalert";
import { useFormik } from "formik";
import * as Yup from "yup";

const dateNow = new Date();
const dateDetails = moment(dateNow).format("DD-MM-YYYY hh:mm");

export default function FormTwoCol() {
  const formik = useFormik({
    initialValues: {
      userName: "",
      fullName: "",
      phoneNumber: "",
      address: "",
      email: "",
      password: "",
      repassword: "",
    },
    validationSchema: Yup.object({
      userName: Yup.string()
        .min(4, "Minimal 4 karakter")
        .max(15, "Maksimal 15 karakter")
        .required("Masukkan username!"),
      fullName: Yup.string()
        .min(4, "Minimal 4 karakter")
        .max(25, "Maksimal 25 karakter")
        .required("Masukkan nama lengkap!"),
      phoneNumber: Yup.number()
        .min(11, "Minimal 11 angka")
        .required("Masukkan nomor HP!"),
      address: Yup.string().required("Masukkan alamat!"),
      email: Yup.string()
        .email("Email tidak sesuai!")
        .required("Masukkan email!"),
      password: Yup.string()
        .min(6, "Minimal 6 karakter")
        .required("Masukkan password!")
        .matches(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/,
          "Minimal 1 Hurf besar, 1 huruf kecil, 1 angka, 6 karakter"
        ),
      repassword: Yup.string()
        .oneOf([Yup.ref("password")], "Password tidak sama!")
        .required("Masukkan kembali password"),
    }),
    onSubmit: () => {
      let post = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify({
          userName: document.getElementById("userName").value,
          fullName: document.getElementById("fullName").value,
          password: document.getElementById("password").value,
          address: document.getElementById("address").value,
          email: document.getElementById("email").value,
          phoneNumber: document.getElementById("phoneNumber").value,
          dateRegist: dateDetails,
          status: {
            id: 1,
          },
        }),
      };
      let send = fetch("http://localhost:8080/save/users", post);
      swal("Data berhasil disimpan!", "Data admin ditambahkan", "success");
      window.setTimeout(function () {
        window.location.reload();
      }, 500);
    },
  });

  return (
    <div class="main">
      <div class="main-content">
        <div class="container-fluid">
          <div class="panel panel-headline">
            <div class="panel-heading">
              <h3 class="panel-title">Tambah Admin</h3>
            </div>
            <div class="panel-body">
              <form onSubmit={formik.handleSubmit}>
                <div class="row">
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Username</h3>
                    </div>
                    <div>
                      <input
                        id="userName"
                        class="form-control"
                        name="userName"
                        type="text"
                        value={formik.values.userName}
                        onChange={formik.handleChange}
                      />
                      {formik.errors.userName && formik.touched.userName && (
                        <p>{formik.errors.userName}</p>
                      )}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Fullname</h3>
                    </div>
                    <div>
                      <input
                        id="fullName"
                        class="form-control"
                        name="fullName"
                        type="text"
                        value={formik.values.fullName}
                        onChange={formik.handleChange}
                      />
                      {formik.errors.fullName && formik.touched.fullName && (
                        <p>{formik.errors.fullName}</p>
                      )}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Nomor HP</h3>
                    </div>
                    <div>
                      <input
                        id="phoneNumber"
                        class="form-control"
                        name="phoneNumber"
                        type="number"
                        value={formik.values.phoneNumber}
                        onChange={formik.handleChange}
                      />
                      {formik.errors.phoneNumber &&
                        formik.touched.phoneNumber && (
                          <p>{formik.errors.phoneNumber}</p>
                        )}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Email</h3>
                    </div>
                    <div>
                      <input
                        id="email"
                        class="form-control"
                        name="email"
                        type="email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                      />
                      {formik.errors.email && formik.touched.email && (
                        <p>{formik.errors.email}</p>
                      )}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Alamat</h3>
                    </div>
                    <div>
                      <textarea
                        id="address"
                        class="form-control"
                        name="address"
                        cols="65%"
                        rows="6"
                        value={formik.values.address}
                        onChange={formik.handleChange}
                      ></textarea>
                      {formik.errors.address && formik.touched.address && (
                        <p>{formik.errors.address}</p>
                      )}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Password</h3>
                    </div>
                    <div>
                      <input
                        id="password"
                        class="form-control"
                        name="password"
                        type="password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                      />
                      {formik.errors.password && formik.touched.password && (
                        <p id="password-err">{formik.errors.password}</p>
                      )}
                    </div>
                    <div>
                      <h3 class="panel-title">Confirm Password</h3>
                    </div>
                    <div>
                      <input
                        id="repassword"
                        class="form-control"
                        name="repassword"
                        type="password"
                        value={formik.values.repassword}
                        onChange={formik.handleChange}
                      />
                    </div>
                    {formik.errors.repassword && formik.touched.repassword && (
                      <p id="repassword-err">{formik.errors.repassword}</p>
                    )}
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Tanggal Registrasi</h3>
                    </div>
                    <div>
                      <input
                        id="dateRegist"
                        class="form-control"
                        name="dateRegist"
                        value={dateDetails}
                        disabled={true}
                      />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div>
                      <h3 class="panel-title">Status</h3>
                    </div>
                    <div>
                      <input
                        id="status"
                        class="form-control"
                        name="status"
                        type="text"
                        value="Admin"
                        disabled={true}
                      />
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">
                    Save
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

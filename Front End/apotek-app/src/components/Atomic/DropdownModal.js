import React, { Component } from "react";
import { Dropdown } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import APIServices from "../../APIServices/APIServices";
import swal from "sweetalert";

export default class DropdownModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModalDelete: false,
      showModalUpdate: false,
      id: this.props.id,
      data: [],
    };
    this.handleOpenModalDelete = this.handleOpenModalDelete.bind(this);
    this.handleCloseModalDelete = this.handleCloseModalDelete.bind(this);
    this.handleOpenModalUpdate = this.handleOpenModalUpdate.bind(this);
    this.handleCloseModalUpdate = this.handleCloseModalUpdate.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.saveEdit = this.saveEdit.bind(this);
    this.delete = this.delete.bind(this);
  }

  async componentDidMount() {
    const response = await APIServices.detailData("obat", this.state.id);
    const body = await response.data;
    this.setState({ data: body });
  }

  handleChange(e) {
    e.preventDefault();
    this.setState((prevState) => ({
      data: {
        ...prevState.data,
        [e.target.name]: e.target.value,
      },
    }));
    console.log(this.state.data);
  }

  handleOpenModalUpdate() {
    this.setState({ showModalUpdate: true });
    console.log(this.state.id);
  }

  handleCloseModalUpdate() {
    this.setState({ showModalUpdate: false });
  }

  handleOpenModalDelete() {
    this.setState({ showModalDelete: true });
  }

  handleCloseModalDelete() {
    this.setState({ showModalDelete: false });
  }

  saveEdit(e) {
    e.preventDefault();
    APIServices.saveData("obats", this.state.data);
    swal("Data berhasil disimpan!", "", "success");
    window.setTimeout(function () {
      window.location.reload();
    }, 800);
  }

  delete(event) {
    event.preventDefault();
    const deleted = {
      method: "DELETE",
      headers: {
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
    };
    const id = this.state.id;
    APIServices.deleteData(this.props.url, id);
    swal(
      "Data berhasil dihapus!",
      "Product yang dipilih telah dihapus!",
      "success"
    );
    this.setState({ showModalUpdate: false });
    window.setTimeout(function () {
      window.location.reload();
    }, 800);
  }

  render() {
    return (
      <>
        <Dropdown>
          <Dropdown.Toggle variant="info" id="dropdown-basic"></Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item onClick={this.handleOpenModalUpdate}>
              <i class="lnr lnr-pencil">
                <span> Edit</span>
              </i>
            </Dropdown.Item>
            <Dropdown.Item onClick={this.handleOpenModalDelete}>
              <i class="lnr lnr-trash">
                <span> Hapus</span>
              </i>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <Modal
          show={this.state.showModalUpdate}
          onHide={this.handleCloseModalUpdate}
          animation={true}
        >
          <Modal.Header>
            <Modal.Title>{this.props.titledetails}</Modal.Title>
          </Modal.Header>
          <Modal.Body className="modal-edit">
            <div class="container-fluid">
              <div class="panel panel-headline">
                <div class="panel-heading">
                  <h3 class="panel-title">Tambah Obat</h3>
                </div>
                <div class="panel-body">
                  <form action="">
                    <div class="row">
                      <div class="col-sm-2">
                        <div>
                          <h3 class="panel-title">ID</h3>
                        </div>
                        <div key={this.state.data.id}>
                          <input
                            id="id"
                            class="form-control"
                            name="id"
                            type="number"
                            value={this.state.data.id}
                          />
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div>
                          <h3 class="panel-title">Nama Obat</h3>
                        </div>
                        <div>
                          <input
                            id="nama-obat"
                            class="form-control"
                            name="nama"
                            value={this.state.data.nama}
                            onChange={this.handleChange}
                            type="text"
                          />
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div>
                          <h3 class="panel-title">Jenis Obat</h3>
                        </div>
                        <div>
                          <input
                            id="jenis"
                            class="form-control"
                            name="jenis"
                            type="text"
                            value={this.state.data.jenis}
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-2">
                        <div>
                          <h3 class="panel-title">Stok</h3>
                        </div>
                        <div>
                          <input
                            id="stok"
                            class="form-control"
                            name="stock"
                            type="number"
                            value={this.state.data.stock}
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div>
                          <h3 class="panel-title">Harga</h3>
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input
                            class="form-control"
                            id="price"
                            name="price"
                            type="number"
                            value={this.state.data.price}
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div>
                          <h3 class="panel-title">Dosis</h3>
                        </div>
                        <div>
                          <input
                            id="dosis"
                            class="form-control"
                            name="dosis"
                            type="text"
                            value={this.state.data.dosis}
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-5">
                        <div>
                          <h3 class="panel-title">Image</h3>
                        </div>
                        <div>
                          <input
                            id="picture"
                            class="form-control"
                            name="picture"
                            type="file"
                          />
                        </div>
                      </div>
                      <div class="col-sm-7">
                        <div>
                          <h3 class="panel-title">Deskripsi</h3>
                        </div>
                        <div>
                          <textarea
                            class="form-control"
                            name="desc"
                            id="desc"
                            cols="65%"
                            rows="5"
                            value={this.state.data.desc}
                            onChange={this.handleChange}
                          ></textarea>
                        </div>
                      </div>
                      <button
                        type="submit"
                        class="btn btn-primary"
                        onClick={this.saveEdit}
                      >
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </Modal.Body>
        </Modal>

        <Modal
          show={this.state.showModalDelete}
          onHide={this.handleCloseModalDelete}
          animation={true}
        >
          <Modal.Header>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>Apakah anda yakin ingin menghapus item ini?</h5>
            <button
              className="btn btn-secondary"
              onClick={this.handleCloseModalDelete}
            >
              Tidak
            </button>
            <button className="btn btn-danger" onClick={this.delete}>
              Ya
            </button>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

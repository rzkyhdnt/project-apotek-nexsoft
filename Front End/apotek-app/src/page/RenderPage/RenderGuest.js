import React, { Component } from "react";
import HomeGuest from "../Guest/HomeGuest";
import NavbarGuest from "../Guest/NavbarGuest";
import ProductContent from "../../components/Member/ProductContent";
import Footer from "../../components/Member/Atomic/Footer";
import DetailsProduct from "../Guest/DetailsProduct";
import CartContainer from "../Guest/CartContainer";

export default function RenderGuest(val) {
  switch (val) {
    case "":
      return <HomeGuest />;
    case "Guest":
      return <HomeGuest />;
    case "Cart":
      return (
        <>
          <NavbarGuest />
          <CartContainer />
        </>
      );
    // case "Resep" : return <Recipe />
    case "Product":
      return (
        <>
          <NavbarGuest />
          <ProductContent />
          <Footer />
        </>
      );
    case "Product Detail":
      return (
        <>
          <NavbarGuest />
          <DetailsProduct />
        </>
      );
      case "Home Guest":
      return (
        <>
          <HomeGuest />
        </>
      );
    default:
      return <HomeGuest />;
  }
}

package com.lastproject.apotek.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import com.lastproject.apotek.model.Cart;
import com.lastproject.apotek.model.Chat;
import com.lastproject.apotek.model.Obat;
import com.lastproject.apotek.model.ObatDTO;
import com.lastproject.apotek.model.Resep;
import com.lastproject.apotek.model.ResepDTO;
import com.lastproject.apotek.model.Status;
import com.lastproject.apotek.model.Transaction;
import com.lastproject.apotek.model.Users;
import com.lastproject.apotek.repository.CartRepo;
import com.lastproject.apotek.repository.ChatRepo;
import com.lastproject.apotek.repository.ObatRepo;
import com.lastproject.apotek.repository.ResepRepo;
import com.lastproject.apotek.repository.StatusRepo;
import com.lastproject.apotek.repository.TransactionRepo;
import com.lastproject.apotek.repository.UsersRepo;
import org.springframework.web.multipart.MultipartFile;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ModelService {
    @Autowired
    private UsersRepo usersRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private TransactionRepo transactionRepo;

    @Autowired
    private ObatRepo obatRepo;

    @Autowired
    private ResepRepo resepRepo;

    @Autowired
    private ChatRepo chatRepo;

    @Autowired
    private CartRepo cartRepo;

    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    // Get All Controller
    public ArrayList<Users> getUsers() {
        return (ArrayList<Users>) usersRepo.findAll();
    }

    public ArrayList<Users> getAllAdmin() {
        return (ArrayList<Users>) usersRepo.findListAdmin();
    }

    public ArrayList<Users> getAllCustomer() {
        return (ArrayList<Users>) usersRepo.findListMember();
    }

    public Users findByUsername(String userName) {
        return usersRepo.findByUserName(userName);
    }

    public ArrayList<Status> getStatus() {
        return (ArrayList<Status>) statusRepo.findAll();
    }

    public ArrayList<Transaction> getResepsList() {
        return (ArrayList<Transaction>) transactionRepo.findResepToAgree();
    }

    public ArrayList<Chat> getChatList() {
        return (ArrayList<Chat>) chatRepo.findAll();
    }

    public ArrayList<Transaction> getTransactions() {
        return (ArrayList<Transaction>) transactionRepo.findAllTransaction();
    }

    public ArrayList<Transaction> getTransactionMember() {
        return (ArrayList<Transaction>) transactionRepo.findMemberTransaction();
    }

    public ArrayList<Transaction> getTransactionGuest() {
        return (ArrayList<Transaction>) transactionRepo.findGuestTransaction();
    }

    public ArrayList<Obat> getObat() {
        return (ArrayList<Obat>) obatRepo.findAllObat();
    }

    public ArrayList<Cart> getCarts() {
        return (ArrayList<Cart>) cartRepo.findAll();
    }

    public ArrayList<Resep> getResep() {
        return (ArrayList<Resep>) resepRepo.findAllReseps();
    }

    // Save Controller
    @Transactional
    public void saveUsers(Users users) {
        if (Pattern.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$", users.getPassword())) {
            users.setPassword(passwordEncoder.encode(users.getPassword()));
            usersRepo.save(users);
        } else {
            System.out.println("Error");
        }
    }

    @Transactional
    public void saveCart(Cart cart) {
        cartRepo.save(cart);
    }

    @Transactional
    public void saveStatus(Status status) {
        statusRepo.save(status);
    }

    @Transactional
    public void saveTransaction(Transaction transaction) {
        transactionRepo.save(transaction);
    }

    @Transactional
    public void saveObat(Obat obat) {
        obatRepo.save(obat);
    }

    @Transactional
    public void saveImageResep(ResepDTO resepDTO) throws IOException {
        ModelMapper mapper = new ModelMapper();
        Resep resep = mapper.map(resepDTO, Resep.class);
        List<MultipartFile> image = resepDTO.getResepImage();
        String listImage = "";

        for (MultipartFile file : image) {
            byte[] bytes = file.getBytes();
            Path path = Paths
                    .get("D:\\Last Project\\Apotek\\Front End\\apotek-app\\public\\" + file.getOriginalFilename());
            Files.write(path, bytes);
            listImage += "/" + file.getOriginalFilename();
        }
        resep.setResepImage(listImage);
        resepRepo.save(resep);
    }

    @Transactional
    public void saveObatDTO(ObatDTO obatDTO) throws IOException {
        ModelMapper mapper = new ModelMapper();
        Obat obat = mapper.map(obatDTO, Obat.class);
        List<MultipartFile> image = obatDTO.getPicture();
        String listPicture = "";

        if (image == null) {
            obatRepo.save(obat);
        } else {
            for (MultipartFile file : image) {
                byte[] bytes = file.getBytes();
                Path path = Paths
                        .get("D:\\Last Project\\Apotek\\Front End\\apotek-app\\public\\" + file.getOriginalFilename());
                Files.write(path, bytes);
                listPicture += "/" + file.getOriginalFilename();
            }

            obat.setPicture(listPicture);
            obatRepo.save(obat);
        }

    }

    @Transactional
    public void saveResep(Resep resep) {
        resepRepo.save(resep);
    }

    @Transactional
    public void saveChat(Chat chat) {
        chatRepo.save(chat);
    }

    // Get Id Controller
    public Optional<Users> getUsersById(Integer id) {
        return usersRepo.findById(id);
    }

    public Optional<Status> getStatusById(Integer id) {
        return statusRepo.findById(id);
    }

    public Optional<Transaction> getTransactionById(Integer id) {
        return transactionRepo.findById(id);
    }

    public Optional<Cart> getCartById(Integer id) {
        return cartRepo.findById(id);
    }

    public Optional<Obat> getObatById(Integer id) {
        return obatRepo.findById(id);
    }

    public Optional<Resep> getResepById(Integer id) {
        return resepRepo.findById(id);
    }

    // Delete Controller
    @Transactional
    public void deleteUsers(Integer id) {
        usersRepo.deleteById(id);
    }

    @Transactional
    public void deleteStatus(Integer id) {
        statusRepo.deleteById(id);
    }

    @Transactional
    public void deleteTransaction(Integer id) {
        transactionRepo.deleteById(id);
    }

    @Transactional
    public void deleteObat(Integer id) {
        obatRepo.deleteObat(id);
    }

    @Transactional
    public void deleteResep(Integer id) {
        resepRepo.deleteById(id);
    }
}

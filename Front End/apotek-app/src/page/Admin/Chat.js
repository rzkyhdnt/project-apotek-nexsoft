import React, { Component } from 'react'
import ChatAdmin from '../../components/Admin/Chat Admin/ChatAdmin'
import NavbarAdmin from '../../components/NavbarAdmin'
import SidebarAdmin from '../../components/SidebarAdmin'
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/main.scss";

export default class Chat extends Component {
    render() {
        return (
            <div id="wrapper">
              <NavbarAdmin />
              <SidebarAdmin />
              <ChatAdmin />  
            </div>
        )
    }
}

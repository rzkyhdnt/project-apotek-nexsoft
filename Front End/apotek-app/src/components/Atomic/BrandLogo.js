import React, { Component } from "react";
import Logo from '../../assets/img/brand/nexmed-logo.png'

export default class BrandLogo extends Component {
  render() {
    return (
      <div class="brand-admin">
        <a href="index.html">
          <img
            src={Logo}
            alt="nexMed Logo"
            className="logo"
          />
        </a>
      </div>
    );
  }
}

import React, { Component } from "react";
import ResepLogo from "../../../assets/img//Users/recipe.png";
import { connect } from "react-redux";

class Recipe extends Component {
  render() {
    return (
      <a class="navbar-brand mr-5 mt-2" href="#" onClick={()=> this.props.setPage("Resep")}>
        <img className="logo-resep" src={ResepLogo} width="30" height="30" alt="" />
      </a>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(Recipe);

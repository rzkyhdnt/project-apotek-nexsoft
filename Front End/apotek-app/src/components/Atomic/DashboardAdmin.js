import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Transaction from "../../assets/img/transaction.png";
import APIServices from "../../APIServices/APIServices";

export default class DashboardAdmin extends Component {
  state = {
    total: 0,
  };

  componentDidMount() {
    APIServices.getData("total").then((resp) =>
      this.setState({ total: resp.data })
    );
  }

  render() {
    const { total } = this.state;
    return (
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">Dashboard</h3>
              </div>
              <div class="panel-body">
                <div className="card transaction">
                  <img id="transaction" src={Transaction} />
                  <h5>Total Transaction</h5>
                  <h4>{total}</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

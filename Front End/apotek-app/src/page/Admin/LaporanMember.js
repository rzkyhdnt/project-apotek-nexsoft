import React, { Component } from "react";
import NavbarAdmin from "../../components/NavbarAdmin";
import SidebarAdmin from "../../components/SidebarAdmin";
import TableLaporan from "../../components/Atomic/TableLaporanMember";

export default class LaporanMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: [
        "Tanggal Beli",
        "Username",
        "Harga Obat",
        "Harga Ongkir",
        "Total Harga",
        "Details",
      ],
    };
  }
  render() {
    const { headers } = this.state;
    return (
      <div id="wrapper">
        <NavbarAdmin />
        <SidebarAdmin />
        <TableLaporan title="Laporan Member" headers={headers} />
      </div>
    );
  }
}

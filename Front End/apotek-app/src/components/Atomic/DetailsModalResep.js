import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import APIServices from "../../APIServices/APIServices";
import swal from "sweetalert";

export default class DetailsModalResep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      id: this.props.id,
      data: [],
      getImg: "",
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.saveEdit = this.saveEdit.bind(this);
  }

  async componentDidMount() {
    const response = await APIServices.detailData("resep", this.state.id);
    const body = await response.data;
    const responseImg = await APIServices.detailData(
      "resep",
      this.state.id + 1
    );
    const bodyImg = await responseImg.data.resepImage;
    this.setState({
      data: body,
      getImg: bodyImg,
    });
  }

  handleChange(e) {
    e.preventDefault();
    this.setState((prevState) => ({
      data: {
        ...prevState.data,
        [e.target.name]: e.target.value,
      },
    }));
    console.log(this.state.data);
  }

  saveEdit(event) {
    event.preventDefault();
    APIServices.saveData("resep", this.state.data);
    swal("Good job!", "You clicked the button!", "success");
    window.setTimeout(function () {
      window.location.reload();
    }, 500);
  }

  openModal() {
    this.setState({ showModal: true });
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { data } = this.state;
    console.log(this.state.getImg);
    return (
      <>
        <div>
          <button className="btn btn-warning" onClick={this.openModal}>
            {this.props.nameButton}
          </button>
        </div>
        <Modal
          show={this.state.showModal}
          onHide={this.closeModal}
          animation={true}
        >
          <Modal.Header>
            <Modal.Title>Update User</Modal.Title>
          </Modal.Header>
          <Modal.Body className="modal-resep">
            <div class="container-fluid">
              <div class="panel panel-headline">
                <div class="panel-body-resep">
                  <h4>Update Resep</h4>
                  <form action="">
                    <div class="col-sm-6">
                      <div>
                        <h3 class="panel-title">Resep Image</h3>
                      </div>
                      <div>
                        <img src={this.state.getImg} alt="Gambar resep" />
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div>
                        <h3 class="panel-title">Isi Resep</h3>
                      </div>
                      <div>
                        <input
                          id="resep-info"
                          class="form-control"
                          onChange={this.handleChange}
                          name="resep-info"
                          type="text"
                          value={data.resepText}
                          readonly
                        />
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div>
                        <h3 class="panel-title">Status</h3>
                      </div>
                      <div>
                        <select
                          id="status-resep"
                          name="status"
                          value={data.status}
                          onChange={this.handleChange}
                        >
                          <option value="Menunggu">Menunggu</option>
                          <option value="Disetujui">Disetujui</option>
                        </select>
                      </div>
                    </div>
                    <hr />
                    <button
                      type="button"
                      class="btn btn-primary"
                      onClick={this.saveEdit}
                    >
                      Save
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

import React, { Component } from "react";
import "../../../assets/vendor/font-awesome/css/font-awesome.css";
import { useTranslation } from "react-i18next";

function Footer() {
  const { t } = useTranslation();
  return (
    <footer>
      <div class="row ml-5">
        <div class="col-sm-3 mt-5">
          <h5>{t("footer.title1")}</h5>
          <div class="dropdown-divider"></div>
          <br />
          <p>Scientia Business Park Lt. 2 Tangerang Selatan</p>
          <i class="fa fa-whatsapp" /> <span>082122116489</span>
        </div>
        <div class="col-sm-3 mt-5">
          <h5>{t("footer.title2.sub1")}</h5>
          <div class="dropdown-divider"></div>
          <br />
          <li>
            <a href="#">{t("footer.title2.sub2")}</a>
          </li>
          <li>
            <a href="#">{t("footer.title2.sub3")}</a>
          </li>
          <li>
            <a href="#">{t("footer.title2.sub4")}</a>
          </li>
          <li>
            <a href="#">{t("footer.title2.sub5")}</a>
          </li>
          <li>
            <a href="#">{t("footer.title2.sub6")}</a>
          </li>
        </div>
        <div class="col-sm-3 mt-5">
          <h5>{t("footer.title3.sub1")}</h5>
          <div class="dropdown-divider"></div>
          <br />
          <li>
            <a href="#">{t("footer.title3.sub2")}</a>
          </li>
          <li>
            <a href="#">{t("footer.title3.sub3")}</a>
          </li>
          <li>
            <a href="#">{t("footer.title3.sub4")}</a>
          </li>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

import React, { Component } from 'react'
import axios from 'axios';

const api_url_get = "http://localhost:8080/";

class APIServices extends Component {
    getData(target){
        const url_target = api_url_get + "get/" + target;
        return axios.get(url_target);
    }

    saveData(target, jsondata){
        const url_target = api_url_get + "save/" + target;
        return axios.post(url_target, jsondata)
    }

    saveEdit(target, jsondata){
        const url_target = api_url_get + "save/" + target;
        return axios.put(url_target, jsondata)
    }

    detailData(target, id){
        const url_target = api_url_get + "get/" + target + "/" + id;
        return axios.get(url_target)
    }

    deleteData(target, id){
        const url_target = api_url_get + "delete/" + target + "/" + id;
        return axios.delete(url_target)
    }
}

export default new APIServices();

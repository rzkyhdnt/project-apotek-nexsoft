package com.lastproject.apotek.repository;

import java.util.List;

import com.lastproject.apotek.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepo extends JpaRepository<Users, Integer> {
    public Users findById(int id);

    @Query(nativeQuery = true, value = "select * from users join status on users.status_id = status.id order by status.name ASC , users.user_name")
    public List<Users> findAll();

    @Query(nativeQuery = true, value = "select * from users join status on users.status_id = status.id where status_id = 1")
    public List<Users> findListAdmin();

    @Query(nativeQuery = true, value = "select * from users join status on users.status_id = status.id where status_id = 2")
    public List<Users> findListMember();

    public Users findByUserName(String userName);

}

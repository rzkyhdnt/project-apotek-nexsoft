const search = window.location;
const query = new URLSearchParams(search).get("s");

const globalState = {
  totalOrder: 0,
  id: 0,
  cart: [],
  allCart: [],
  jumlahPerProduct: [],
  totalPrice: 0,
  orderPerProduct: 0,
  isLogin: "",
  status: "",
  page: "",
  search: query,
};

const rootReducer = (state = globalState, action) => {
  switch (action.type) {
    case "ADD_CART":
      return {
        ...state,
        totalOrder: state.totalOrder + 1,
        orderPerProduct: state.orderPerProduct + 1,
      };
    case "REMOVE_TOTAL_ORDER": {
      return {
        ...state,
        totalOrder: action.value,
      };
    }
    case "RESET_TOTAL_ORDER": {
      return {
        ...state,
        totalOrder: 0,
      };
    }
    case "MINUS_CART":
      if (state.totalOrder > 0 && state.orderPerProduct > 0) {
        return {
          ...state,
          totalOrder: state.totalOrder - 1,
          orderPerProduct: state.orderPerProduct - 1,
        };
      } else {
        return {
          ...state,
          totalOrder: 0,
          orderPerProduct: 0,
        };
      }
    case "HANDLE_CHANGE":
      if (state.totalOrder > 0 && state.orderPerProduct > 0) {
        return {
          ...state,
          totalOrder: action.value,
          orderPerProduct: action.value,
        };
      } else {
        return {
          ...state,
          totalOrder: 0,
          orderPerProduct: 0,
        };
      }

    case "GET_ID":
      return {
        ...state,
        id: action.value,
      };
    case "ADD_TO_CART": {
      return {
        ...state,
        cart: [...state.cart, action.value],
      };
    }
    case "GET_TOTAL_ORDER": {
      return {
        ...state,
        totalOrder: action.value,
      };
    }
    case "GET_CART": {
      return {
        ...state,
        cart: action.value,
      };
    }
    case "GET_ALL_DATA": {
      return {
        ...state,
        allCart: action.value,
      };
    }
    case "REMOVE_ALL_CART": {
      return {
        ...state,
        cart: [],
      };
    }
    case "GET_TOTAL_PRICE": {
      return {
        ...state,
        totalPrice: action.value,
      };
    }
    case "GET_ORDER_PRODUCT": {
      return {
        ...state,
        jumlahPerProduct: [...state.jumlahPerProduct, action.value],
      };
    }
    case "DELETE_ITEM": {
      return {
        ...state,
        cart: state.cart.filter((item) => item.id !== action.value),
      };
    }
    case "DELETE_QTY_PRODUCT": {
      return {
        ...state,
        jumlahPerProduct: state.jumlahPerProduct.filter(
          (item) => item !== action.value
        ),
      };
    }
    case "DELETE_ORDER": {
      return {
        ...state,
        totalOrder: state.totalOrder - action.value,
      };
    }
    case "LOGIN_REQUEST": {
      return {
        ...state,
        isLogin: action.value,
      };
    }
    case "LOGOUT_REQUEST": {
      return {
        ...state,
        isLogin: false,
        status: "",
        page: "",
      };
    }
    case "GET_STATUS": {
      return {
        ...state,
        status: action.value,
      };
    }
    case "SET_PAGE": {
      return {
        ...state,
        page: action.value,
      };
    }
    case "SEARCH_ITEM": {
      return {
        ...state,
        search: action.value,
      };
    }
  }
  return state;
};

export default rootReducer;

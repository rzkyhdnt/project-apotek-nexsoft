package com.lastproject.apotek.repository;

import com.lastproject.apotek.model.Chat;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatRepo extends JpaRepository<Chat, Integer> {
    
}

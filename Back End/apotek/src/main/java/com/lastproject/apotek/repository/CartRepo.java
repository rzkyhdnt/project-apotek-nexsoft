package com.lastproject.apotek.repository;

import com.lastproject.apotek.model.Cart;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepo extends JpaRepository<Cart, Integer> {
    public Cart findById(int id);
}

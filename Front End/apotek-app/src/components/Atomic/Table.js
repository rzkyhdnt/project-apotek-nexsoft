import React, { Component } from "react";

export default class Table extends Component {
  render() {
    return (
      <div class="panel-body">
        <table class="table table-hover">
          <thead>
            <tr>
              {headers.map((item) => (
                <th key={item.id} scope="col">
                  {item}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {tableContent.map((item) => (
              <tr key={item.id}>
                <td>{item.date}</td>
                <td>{item.users.userName}</td>
                <td>{item.price}</td>
                <td>{item.deliverPrice}</td>
                <td>{item.totalPrice}</td>
                <td>
                  <DropdownModal />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

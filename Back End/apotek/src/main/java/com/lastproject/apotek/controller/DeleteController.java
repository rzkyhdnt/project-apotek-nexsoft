package com.lastproject.apotek.controller;

import com.lastproject.apotek.service.ModelService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/delete")
public class DeleteController {
    @Autowired
    ModelService modelService;

    @DeleteMapping("/obat/{id}")
    public void deleteObat(@PathVariable("id") int id){
        modelService.deleteObat(id);
    }
}

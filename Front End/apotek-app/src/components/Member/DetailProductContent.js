import React, { Component } from "react";
import Navbar from "./Navbar";
import HeaderInfo from "./HeaderInfo";
import DetailsProduct from "./Atomic/DetailsProduct";
import Footer from "./Atomic/Footer";
import MultiCarousel from "./Atomic/MultiCarousel";

export default class DetailProductContent extends Component {
  render() {
    return (
      <>
        <Navbar />
        <HeaderInfo />
        <DetailsProduct />
        <Footer />
      </>
    );
  }
}

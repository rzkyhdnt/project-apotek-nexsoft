import React, { Component } from "react";
import FormTwoCol from "../../components/Atomic/FormTwoCol";

import NavbarAdmin from "../../components/NavbarAdmin";
import SidebarAdmin from "../../components/SidebarAdmin";

export default class TambahAdmin extends Component {
  render() {
    return (
      <div id="wrapper">
        <NavbarAdmin />
        <SidebarAdmin />
        <FormTwoCol />
      </div>
    );
  }
}

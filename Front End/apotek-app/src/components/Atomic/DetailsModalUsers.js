import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import APIServices from "../../APIServices/APIServices";
import swal from "sweetalert";
import { Formik } from "formik";
import * as Yup from "yup";

const schema = Yup.object({
  userName: Yup.string()
    .min(4, "Minimal 4 karakter")
    .max(15, "Maksimal 15 karakter")
    .required("Masukkan username!"),
  fullName: Yup.string()
    .min(4, "Minimal 4 karakter")
    .max(25, "Maksimal 25 karakter")
    .required("Masukkan nama lengkap!"),
  phoneNumber: Yup.number()
    .min(11, "Minimal 11 angka")
    .required("Masukkan nomor HP!"),
  address: Yup.string().required("Masukkan alamat!"),
  email: Yup.string().email("Email tidak sesuai!").required("Masukkan email!"),
  password: Yup.string()
    .min(6, "Minimal 6 karakter")
    .required("Masukkan password!"),
  repassword: Yup.string()
    .oneOf([Yup.ref("password")], "Password tidak sama!")
    .required("Masukkan kembali password"),
});

export default class DetailsModalUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      id: this.props.id,
      data: [],
      status: [],
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.saveEdit = this.saveEdit.bind(this);
  }

  async componentDidMount() {
    const response = await APIServices.detailData("users", this.state.id);
    const body = await response.data;
    this.setState({ data: body, status: body.status.name });
  }

  handleChange(e) {
    e.preventDefault();
    this.setState((prevState) => ({
      data: {
        ...prevState.data,
        [e.target.name]: e.target.value,
      },
    }));
    console.log(this.state.data);
  }

  saveEdit(event) {
    event.preventDefault();
  }

  openModal() {
    const status = this.state.data.status.name;
    console.log(status);
    if (status !== "Admin") {
      this.setState({ showModal: true });
    } else {
      swal("Anda tidak dapat mengedit user dengan status Admin");
    }
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { data, id } = this.state;
    return (
      <>
        <div>
          <button className="btn btn-warning" onClick={this.openModal}>
            {this.props.nameButton}
          </button>
        </div>
        <Modal
          show={this.state.showModal}
          onHide={this.closeModal}
          animation={true}
        >
          <Modal.Body className="modal-edit">
            <div class="container-fluid">
              <div class="panel panel-headline">
                <div class="panel-body">
                  <h4>Update Users</h4>
                  <Formik
                    initialValues={{
                      userName: data.userName,
                      fullName: data.fullName,
                      phoneNumber: data.phoneNumber,
                      address: data.address,
                      email: data.email,
                      dateRegist: data.dateRegist,
                      password: data.password,
                      repassword: data.repassword,
                      status: data.status,
                    }}
                    validationSchema={schema}
                    onSubmit={() => {
                      let post = {
                        method: "POST",
                        headers: {
                          "Content-Type": "application/json; charset=UTF-8",
                        },
                        body: JSON.stringify({
                          id:this.state.id,
                          userName: document.getElementById("userName").value,
                          fullName: document.getElementById("fullName").value,
                          password: document.getElementById("password").value,
                          address: document.getElementById("address").value,
                          email: document.getElementById("email").value,
                          phoneNumber:
                            document.getElementById("phoneNumber").value,

                          dateRegist: document.getElementById("dateRegist").value,
                          status: {
                            id : document.getElementById("status2").value
                          }
                        }),
                      };
                      let send = fetch(
                        "http://localhost:8080/save/users/",
                        post
                      );
                      swal("Data berhasil disimpan!", "Data admin ditambahkan", "success")
                    }}
                  >
                    {(props) => {
                      const {
                        touched,
                        errors,
                        handleSubmit,
                        values,
                        handleChange,
                      } = props;
                      return (
                        <form onSubmit={handleSubmit}>
                          <div class="row">
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Username</h3>
                              </div>
                              <div>
                                <input
                                  id="userName"
                                  class="form-control"
                                  name="userName"
                                  value={values.userName}
                                  onChange={handleChange}
                                  type="text"
                                />
                                {errors.userName && touched.userName && (
                                  <p>{errors.userName}</p>
                                )}
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Fullname</h3>
                              </div>
                              <div>
                                <input
                                  id="fullName"
                                  class="form-control"
                                  name="fullName"
                                  onChange={handleChange}
                                  value={values.fullName}
                                  type="text"
                                />
                                {errors.fullName && touched.fullName && (
                                  <p>{errors.fullName}</p>
                                )}
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Nomor HP</h3>
                              </div>
                              <div>
                                <input
                                  id="phoneNumber"
                                  class="form-control"
                                  name="phoneNumber"
                                  onChange={handleChange}
                                  type="number"
                                  value={values.phoneNumber}
                                />
                                {errors.phoneNumber && touched.phoneNumber && (
                                  <p>{errors.phoneNumber}</p>
                                )}
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Email</h3>
                              </div>
                              <div>
                                <input
                                  id="email"
                                  class="form-control"
                                  name="email"
                                  onChange={handleChange}
                                  type="email"
                                  value={values.email}
                                />
                                {errors.email && touched.email && (
                                  <p>{errors.email}</p>
                                )}
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Alamat</h3>
                              </div>
                              <div>
                                <textarea
                                  id="address"
                                  class="form-control"
                                  name="address"
                                  cols="65%"
                                  onChange={handleChange}
                                  rows="6"
                                  value={values.address}
                                />
                                {errors.address && touched.address && (
                                  <p>{errors.address}</p>
                                )}
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Password</h3>
                              </div>
                              <div>
                                <input
                                  id="password"
                                  class="form-control"
                                  onChange={handleChange}
                                  name="password"
                                  type="password"
                                  value={values.password}
                                />
                                {errors.password && touched.password && (
                                  <p>{errors.password}</p>
                                )}
                              </div>
                              <div>
                                <h3 class="panel-title">Confirm Password</h3>
                              </div>
                              <div>
                                <input
                                  id="repassword"
                                  class="form-control"
                                  type="password"
                                  value={values.repassword}
                                  onChange={handleChange}
                                />
                                {errors.repassword && touched.repassword && (
                                  <p id="repassword-err">{errors.repassword}</p>
                                )}
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Tanggal Registrasi</h3>
                              </div>
                              <div>
                                <input
                                  id="dateRegist"
                                  class="form-control"
                                  onChange={this.handleChange}
                                  name="dateRegist"
                                  value={values.dateRegist}
                                  disabled
                                />
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div>
                                <h3 class="panel-title">Status</h3>
                              </div>
                              <div>
                              <input
                                  id="status2"
                                  name="status"
                                  type="number"
                                  value={values.status.id}
                                  hidden
                                />
                                <input
                                  class="form-control"
                                  id="status"
                                  name="status"
                                  type="text"
                                  value={values.status.name}
                                  disabled
                                />
                              </div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                              Save
                            </button>
                          </div>
                        </form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

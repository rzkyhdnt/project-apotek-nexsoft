import React, { Component } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/main.scss";
import NavbarAdmin from '../../components/NavbarAdmin'
import SidebarAdmin from '../../components/SidebarAdmin'
import DashboardAdmin from '../../components/Atomic/DashboardAdmin';

export default class Dashboard extends Component {
    render() {
        return (
            <div id="wrapper">
                <NavbarAdmin />
                <SidebarAdmin />
                <DashboardAdmin />
            </div>
        )
    }
}

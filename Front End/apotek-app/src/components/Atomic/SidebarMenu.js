import React, { Component } from "react";
import { Accordion, Card } from "react-bootstrap";
import "../../assets/vendor/linearicons/style.css";
import { connect } from "react-redux";

class SidebarMenu extends Component {

  render() {
    return (
      <div className="sidebar-menu">
        <Accordion>
          <Card className="card-single">
            <Card.Body>
              <a href="#" onClick={() => this.props.setPage("Dashboard")}>
                <i class="lnr lnr-home"></i>
                <span>Dashboard</span>
              </a>
            </Card.Body>
          </Card>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="0">
              <i class="lnr lnr-heart-pulse"></i> <span>Obat</span>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
                <ul class="nav">
                  <li>
                    <a href="#" class="" onClick={() => this.props.setPage("Tambah Obat")}>
                      Tambah Obat
                    </a>
                  </li>
                  <li>
                    <a href="#" class="" onClick={() => this.props.setPage("Daftar Obat")}>
                      Daftar Obat
                    </a>
                  </li>
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="1">
              <i class="lnr lnr-user"></i> <span>Users</span>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="1">
              <Card.Body>
                <ul class="nav">
                  <li>
                    <a href="#" class="" onClick={() => this.props.setPage("Tambah Admin")}>
                      Tambah Admin
                    </a>
                  </li>
                  <li>
                    <a href="#" class="" onClick={() => this.props.setPage("Daftar Users")}>
                      Data Users
                    </a>
                  </li>
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="2">
              <i class="lnr lnr-book"></i> <span>Laporan</span>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="2">
              <Card.Body>
                <ul class="nav">
                  <li>
                    <a href="#" class="" onClick={() => this.props.setPage("Laporan Member")}>
                      Member
                    </a>
                  </li>
                  <li>
                    <a href="#" class="" onClick={() => this.props.setPage("Laporan Nonmember")}>
                      Non Member
                    </a>
                  </li>
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card className="card-single">
            <Card.Body>
              <a href="#" onClick={() => this.props.setPage("Resep")}>
                <i class="lnr lnr-paperclip" />
                <span>Resep</span>
              </a>
            </Card.Body>
          </Card>
          <Card className="card-single">
            <Card.Body>
              <a href="#" onClick={() => this.props.setPage("Chat Admin")}>
                <i class="lnr lnr-envelope" />
                <span>Chat</span>
              </a>
            </Card.Body>
          </Card>
        </Accordion>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(SidebarMenu);
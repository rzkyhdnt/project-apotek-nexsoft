import React, { Component } from "react";
import "../../../assets/vendor/font-awesome/css/font-awesome.css";
import { connect } from "react-redux";

class SearchBar extends Component {
  render() {
    console.log(this.props.search);
    return (
      <form action="/home" class="form-inline" method="get">
        <input
          class="form-control"
          type="search"
          placeholder="Search"
          aria-label="Search"
          value={this.props.search}
          onInput={e => this.props.handleChange(e.target.value)}
          name="s"
        />
        <button class="btn" type="submit">
          <i class="fa fa-search"></i>
        </button>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    search: state.search,
  };
};

const mapDispatch = (dispatch) => {
  return {
    handleChange: (val) => dispatch({ type: "SEARCH_ITEM", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(SearchBar);

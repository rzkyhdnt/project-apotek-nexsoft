import React, { Component } from "react";
import SearchBar from "../../components/Member/Atomic/SearchBar";
import CartIcon from "../../components/Member/Atomic/CartIcon";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import BrandLogoGuest from "../../components/Member/Atomic/BrandLogoGuest";

function NavbarGuest() {
  const { t } = useTranslation();

  return (
    <nav className="navbar navbar-expand-lg navbar-light">
      <BrandLogoGuest />
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <SearchBar />
        <ul className="navbar-nav ml-auto">
          <li>
            <CartIcon />
          </li>
          <li className="nav-item dropdown">
            <Link to="/login">{t("navbar")}</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default NavbarGuest;

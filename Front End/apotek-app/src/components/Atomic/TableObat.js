import React, { Component } from "react";
import APIService from "../../APIServices/APIServices"
import DropdownModal from "./DropdownModal";

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: this.props.headers,
      data: [],
    };
  }

  async componentDidMount() {
    const response = await APIService.getData("obat");
    const body = await response.data;
    this.setState({ data: body });
  }

  render() {
    const { headers, data } = this.state;
    return (
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">{this.props.title}</h3>
              </div>
              <div class="panel-body">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      {headers.map((item) => (
                        <th key={item.id} scope="col">
                          {item}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((item) => (
                      <tr key={item.id}>
                        <td><img src={item.picture}/></td>
                        <td>{item.nama}</td>
                        <td>{item.jenis}</td>
                        <td>{item.stock}</td>
                        <td className="td-harga">{"Rp. " + item.price}</td>
                        <td className="td-dosis">{item.dosis}</td>
                        <td className="td-desc">{item.desc}</td>
                        <td><DropdownModal title="Delete Obat" id={item.id} url={"obat"} titledetails={"Details Obat"} /></td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import Form from "../../components/Atomic/Form";
import NavbarAdmin from "../../components/NavbarAdmin";
import SidebarAdmin from "../../components/SidebarAdmin";

export default class TambahObat extends Component {
  render() {
    return (
      <div id="wrapper">
        <NavbarAdmin />
        <SidebarAdmin />
        <Form />
      </div>
    );
  }
}

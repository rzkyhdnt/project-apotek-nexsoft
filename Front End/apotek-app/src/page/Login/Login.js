import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import {
  Row,
  Col,
  Card,
  Form,
  InputGroup,
  FormControl,
  Button,
} from "react-bootstrap";
import { connect } from "react-redux";
import "../../assets/vendor/font-awesome/css/font-awesome.css";
import "../../assets/css/loginregister.scss";
import APIServices from "../../APIServices/APIServices";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: "",
      userName: "",
      password: "",
      temp: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.loginUser = this.loginUser.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    APIServices.getData("cart").then((resp) => {
      this.setState({ temp: resp.data }, () => {
        this.props.getAllDataAfterFetch(this.state.temp);
      });
    });
  }

  loginUser() {
    fetch(
      `http://localhost:8080/save/auth?userName=${this.state.userName}&password=${this.state.password}`,
      {
        method: "POST",
        headers: { "Content-type": "application/json; charset=UTF-8" },
      }
    )
      .then((resp) => {
        if (!resp.ok) {
          this.setState({
            errorMessage: "Username atau password ada yang salah!",
          });
        }
        return resp.json();
      })
      .then((resp) => {
        this.props.sendToLogin(true);
        this.props.getStatus(resp.status.name);
        this.setState({ errorMessage: "" });
        localStorage.setItem("username", resp.username);
        localStorage.setItem("id", resp.id);

        this.state.temp.map((item) => {
          if (item.user.id == localStorage.getItem("id")) {
            this.props.getTotalOrder(item.jumlah);
            this.props.getCart(item.obat);
            window.setTimeout(function () {
              window.location.reload();
            }, 1000);
          } else {
            console.log("Nothing");
          }
        });
      });
  }

  render() {
    const { password, userName } = this.state;
    const { isLogin } = this.props;
    if (!isLogin) {
      return (
        <div id="login">
          <Row className="justify-content-md-center">
            <Col xs={4}>
              <Card>
                <Card.Header>Login</Card.Header>
                <Card.Body>
                  <Form.Row>
                    <Form.Group>
                      <InputGroup>
                        <InputGroup.Prepend>
                          <InputGroup.Text>
                            <i className="fa fa-user" />{" "}
                          </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                          required
                          autoComplete="off"
                          type="text"
                          name="userName"
                          placeholder="Enter Your Email"
                          value={userName}
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group>
                      <InputGroup>
                        <InputGroup.Prepend>
                          <InputGroup.Text>
                            <i className="fa fa-key" />
                          </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                          required
                          autoComplete="off"
                          type="password"
                          name="password"
                          placeholder="Enter Your Password"
                          value={password}
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                    </Form.Group>
                    <p>{this.state.errorMessage}</p>
                  </Form.Row>
                  <Button size="sm" type="button" onClick={this.loginUser}>
                    Login
                  </Button>
                </Card.Body>
                <Card.Footer>
                  <Link to="/register">Belum punya akun? Register</Link>
                </Card.Footer>
              </Card>
            </Col>
          </Row>
        </div>
      );
    } else {
      return <Redirect to={"/home"} />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: state.isLogin,
    status: state.status,
    cart: state.cart,
    allCart: state.allCart,
  };
};

const mapToProps = (dispatch) => {
  return {
    sendToLogin: (val) => dispatch({ type: "LOGIN_REQUEST", value: val }),
    getStatus: (val) => dispatch({ type: "GET_STATUS", value: val }),
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
    getCart: (val) => dispatch({ type: "GET_CART", value: val }),
    getTotalOrder: (val) => dispatch({ type: "GET_TOTAL_ORDER", value: val }),
    getAllDataAfterFetch: (val) =>
      dispatch({ type: "GET_ALL_DATA", value: val }),
  };
};

export default connect(mapStateToProps, mapToProps)(Login);

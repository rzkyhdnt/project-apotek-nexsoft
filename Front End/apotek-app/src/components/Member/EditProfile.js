import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import APIServices from "../../APIServices/APIServices";
import swal from "sweetalert";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    const response = await APIServices.detailData(
      "users",
      localStorage.getItem("id")
    );
    const body = await response.data;
    this.setState({ data: body });
  }

  handleChange(e) {
    this.setState({
      data: {
        [e.target.name]: e.target.value,
      },
    });
  }

  send(e) {
    let post = {
      method: "PUT",
      headers: { "Content-Type": "application/json; charset=UTF-8" },
      body: JSON.stringify({
        id: document.getElementById("id").value,
        userName: document.getElementById("userName").value,
        fullName: document.getElementById("fullName").value,
        address: document.getElementById("address").value,
        email: document.getElementById("email").value,
        phoneNumber: document.getElementById("phoneNumber").value,
      }),
    };
    let send = fetch(
      "http://localhost:8080/save/users/" + document.getElementById("id").value,
      post
    );
    swal("Edit Berhasil", "Data berhasil diedit!", "success");
    localStorage.setItem("username", document.getElementById("userName").value);
    this.props.setPage(e);
  }

  render() {
    const { data } = this.state;
    const { t } = this.props;
    return (
      <div className="main-content">
        <div className="side-content">
          <div class="row profile">
            <a href="#" onClick={() => this.props.setPage("Edit Profile")}>
              {t("user.side.profil")}
            </a>
          </div>
          <div class="row">
            <a href="#" onClick={() => this.props.setPage("History Transaksi")}>
              {t("user.side.history")}
            </a>
          </div>
          <div class="row">
            <a href="#" onClick={() => this.props.setPage("Edit Password")}>
              {t("user.side.password")}
            </a>
          </div>
        </div>
        <div className="divider-vertical"></div>
        <div className="form-content">
          <h4>{t("user.side.profil")}</h4>
          <div className="row">
            <div className="col-10">
              <h5 hidden>ID</h5>
            </div>
            <div className="col-2">
              <input type="text" id="id" value={data.id} hidden />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <h5>{t("user.main.profil.username")}</h5>
            </div>
            <div className="col-8">
              <input
                value={data.userName}
                name="userName"
                id="userName"
                onChange={this.handleChange}
                type="text"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <h5>{t("user.main.profil.fullname")}</h5>
            </div>
            <div className="col-8">
              <input
                type="text"
                id="fullName"
                value={data.fullName}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <h5>{t("user.main.profil.phoneNumber")}</h5>
            </div>
            <div className="col-8">
              <input
                type="number"
                id="phoneNumber"
                value={data.phoneNumber}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <h5>Email</h5>
            </div>
            <div className="col-8">
              <input
                type="email"
                id="email"
                value={data.email}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-3 address">
              <h5>{t("user.main.profil.address")}</h5>
            </div>
            <div className="col-8">
              <textarea
                type="text"
                id="address"
                value={data.address}
                onChange={this.handleChange}
              />
            </div>
            <div className="col-12">
              <button
                className="btn btn-primary"
                type="submit"
                onClick={() => this.send("Home")}
              >
                {t("user.main.profil.button")}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatch
)(withTranslation()(EditProfile));

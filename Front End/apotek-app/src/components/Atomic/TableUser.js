import React, { Component } from "react";
import APIService from "../../APIServices/APIServices";
import DetailsModalUsers from "./DetailsModalUsers";
import SearchBar from "./SearchBar";

const { search } = window.location;
const query = new URLSearchParams(search).get("s");

export default class TableUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: this.props.headers,
      data: [],
      searchQuery: query,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    const response = await APIService.getData("users");
    const body = await response.data;
    this.setState({ data: body });
  }

  handleChange(param) {
    this.setState({ searchQuery: param });
  }

  render() {
    const { headers, data } = this.state;
    const filterPosts = (data, query) => {
      if (!query) {
        return data;
      }

      return data.filter((post) => {
        const statuslower = post.status.name.toLowerCase().includes(query),
          userNameLower = post.userName.toLowerCase().includes(query),
          statusUpper = post.status.name.toUpperCase().includes(query),
          userNameUpper = post.userName.toUpperCase().includes(query);
        return statuslower || userNameLower || statusUpper || userNameUpper;
      });
    };
    const filteredPost = filterPosts(data, this.state.searchQuery);

    return (
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">{this.props.title}</h3>
              </div>
              <div class="panel-body">
                <SearchBar
                  searchQuery={this.state.searchQuery}
                  setSearchQuery={this.handleChange}
                />
                <table class="table table-hover">
                  <thead>
                    <tr>
                      {headers.map((item) => (
                        <th key={item.id} scope="col">
                          {item}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {filteredPost.map((item) => (
                      <tr key={item.id}>
                        <td>{item.userName}</td>
                        <td>{item.fullName}</td>
                        <td>{item.address}</td>
                        <td>{item.phoneNumber}</td>
                        <td>{item.email}</td>
                        <td>{item.dateRegist}</td>
                        <td>{item.status.name}</td>
                        <td>
                          <DetailsModalUsers
                            nameButton={"Update"}
                            id={item.id}
                          />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import Carousel from "react-bootstrap/Carousel";
import Image1 from "../../assets/img/Users/img-slider1.jpg";
import Image2 from "../../assets/img/Users/img-slider2.jpg";
import Image3 from "../../assets/img/Users/img-slider3.jpg";
import Image4 from "../../assets/img/Users/img-slider4.jpg";

export default class CarouselHeader extends Component {
  render() {
    return (
      <>
        <Carousel variant="dark" fade interval={1500} autoplay>
          <Carousel.Item>
            <img className="d-block" src={Image1} alt="First slide" />
          </Carousel.Item>
          <Carousel.Item>
            <img className="d-block" src={Image2} alt="Second slide" />
          </Carousel.Item>
          <Carousel.Item>
            <img className="d-block" src={Image3} alt="Third slide" />
          </Carousel.Item>
          <Carousel.Item>
            <img className="d-block" src={Image4} alt="Third slide" />
          </Carousel.Item>
        </Carousel>
        <div class="divider"></div>
      </>
    );
  }
}

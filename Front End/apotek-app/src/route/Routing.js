import React, { Component } from "react";
import { connect } from "react-redux";
import RenderMember from "../page/RenderPage/RenderMember";
import RenderAdmin from "../page/RenderPage/RenderAdmin";
import RenderGuest from "../page/RenderPage/RenderGuest";

class Routing extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    if (this.props.status === "Member") {
      return <>{RenderMember(this.props.page)}</>;
    } else if (this.props.status === "" || this.props.status === "Guest") {
      return <>{RenderGuest(this.props.page)}</>;
    } else if (this.props.status === "Admin") {
      return <>{RenderAdmin(this.props.page)}</>;
    } else if (this.props.isLogin) {
      return <>{RenderMember(this.props.page)}</>;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: state.isLogin,
    status: state.status,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(Routing);

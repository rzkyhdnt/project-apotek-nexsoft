import React, { Component } from "react";
import { connect } from "react-redux";
import { Table } from "react-bootstrap";
import APIServices from "../../APIServices/APIServices";
import { withTranslation } from "react-i18next";

class HistoryTransaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    APIServices.getData("transaction/member").then((resp) => {
      this.setState({ data: resp.data });
    });
  }

  render() {
    console.log(this.state.data);
    const { data } = this.state;
    const { t } = this.props;
    return (
      <div className="main-content">
        <div className="side-content">
          <div class="row">
            <a href="#" onClick={() => this.props.setPage("Edit Profile")}>
              {t("user.side.profil")}
            </a>
          </div>
          <div class="row history">
            <a href="#" onClick={() => this.props.setPage("History Transaksi")}>
              {t("user.side.history")}
            </a>
          </div>
          <div class="row">
            <a href="#" onClick={() => this.props.setPage("Edit Password")}>
              {t("user.side.password")}
            </a>
          </div>
        </div>
        <div className="form-content">
          <h4>{t("user.side.history")}</h4>
          <Table responsive="sm">
            <thead>
              <tr>
                <th>No.</th>
                <th>{t("user.main.history.date")}</th>
                <th>{t("user.main.history.totalItems")}</th>
                <th>{t("user.main.history.itemPrice")}</th>
                <th>{t("user.main.history.deliverPrice")}</th>
                <th>{t("user.main.history.totalPrice")}</th>
                <th>{t("user.main.history.items")}</th>
              </tr>
            </thead>
            <tbody>
              {data.map((item, index) => {
                return (
                  <>
                    <tr>
                      <td key={index}>{index + 1}</td>
                      <td>{item.date}</td>
                      <td>{item.quantity}</td>
                      <td>Rp {item.price}</td>
                      <td>Rp {item.deliverPrice}</td>
                      <td>Rp {item.totalPrice}</td>
                      <td id="nama-obat">
                        {item.obat.map((obat) => {
                          return <li>{obat.nama}</li>;
                        })}
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatch
)(withTranslation()(HistoryTransaction));

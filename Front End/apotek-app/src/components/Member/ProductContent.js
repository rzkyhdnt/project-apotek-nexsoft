import React, { Component } from "react";
import APIServices from "../../APIServices/APIServices";
import ReactPaginate from "react-paginate";
import { connect } from "react-redux";
import {withTranslation} from "react-i18next";

class ProductContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      data: [],
      perPage: 10,
      currentPage: 0,
      id: 0,
    };
    this.handlePageClick = this.handlePageClick.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    this.props.setPage("Product Detail");
    this.props.handleClick(e);
  }

  receivedData() {
    APIServices.getData("obat").then((res) => {
      const data = res.data;
      const search = this.props.search;
      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );
      const filterPosts = (slice, search) => {
        if (!search) {
          return slice;
        }
        return data.filter((post) => {
          const productName = post.nama.toLowerCase().includes(search);
          return productName;
        });
      };
      const filteredPost = filterPosts(slice, search);
      const content = filteredPost.map((item, index) => (
        <>
          <a
            href="#"
            class="thumb"
            key={index}
            id={item.id}
            onClick={() => this.handleClick(item.id)}
          >
            <div class="card mt-3">
              <img class="card-img-top" src={item.picture} alt={item.nama} />
              <div class="card-body">
                <h5 class="card-text">{item.nama}</h5>
                <h5 class="price">
                  Rp. {this.formatNumber(item.price)} <span>/ pcs</span>
                </h5>
              </div>
            </div>
          </a>
        </>
      ));
      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        content,
      });
    });
  }

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData();
      }
    );
  };

  componentDidMount() {
    this.receivedData();
  }

  formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }

  render() {
    const {t} = this.props;
    return (
      <div class="main-content">
        <div class="title mt-5 ml-5">
          <h4 onClick>{t("home.title1")}</h4>
        </div>
        <div className="row ml-5">
          {this.state.content}
          <ReactPaginate
            previousLabel={"<"}
            nextLabel={">"}
            breakLabel={"..."}
            pageCount={this.state.pageCount}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.id,
    page: state.page,
    search : state.search
  };
};

const MapDispatch = (dispatch) => {
  return {
    handleClick: (val) => dispatch({ type: "GET_ID", value: val }),
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, MapDispatch)(withTranslation()(ProductContent));

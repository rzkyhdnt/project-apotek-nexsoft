import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/customer.scss";
import Navbar from "../../components/Member/Navbar";
import HeaderInfo from "../../components/Member/HeaderInfo";
import CarouselHeader from "../../components/Member/CarouselHeader";
import HomeContent from "../../components/Member/HomeContent";
import Footer from "../../components/Member/Atomic/Footer";

export default class Home extends Component {
  render() {
    return (
      <>
        <Navbar />
        <HeaderInfo />
        <CarouselHeader />
        <HomeContent />
        <Footer />
      </>
    );
  }
}

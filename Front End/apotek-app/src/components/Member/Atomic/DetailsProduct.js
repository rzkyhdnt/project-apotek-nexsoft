import React, { Component } from "react";
import APIServices from "../../../APIServices/APIServices";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import swal from "sweetalert";

class DetailsProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      cart: [],
      stock: 0,
    };
    this.addToCart = this.addToCart.bind(this);
    this.minusStock = this.minusStock.bind(this);
    this.addStock = this.addStock.bind(this);
  }

  componentDidMount() {
    APIServices.detailData("obat", this.props.id).then((res) => {
      const data = res.data;
      this.setState({ data: data, stock: data.stock });
    });
  }

  minusStock = () => {
    this.props.addCart();
    this.setState({ stock: this.state.stock - 1 });
  };

  addStock = () => {
    const { data, stock } = this.state;
    this.props.minusCart();
    if (stock !== 0) {
      this.setState({ stock: this.state.stock + 1 });
      if (data.stock == stock) {
        this.setState({ stock: data.stock });
      }
    } else {
      this.setState({ stock: stock + 1 });
    }
  };

  showStock = () => {
    const { stock } = this.state;
    const { t } = this.props;
    if (stock > 0) {
      return (
        <>
          <h5 class="stok">
            {t("productDetails.stock")} {stock}
          </h5>
          <button className="add-minus" onClick={() => this.addStock()}>
            -
          </button>
          <input
            type="number"
            class="quantity"
            value={this.props.orderPerProduct}
            onChange={() => this.handleChange}
            name="jumlah"
          />
          <button className="add-minus" onClick={() => this.minusStock()}>
            +
          </button>
          <button class="buy" onClick={this.addToCart}>
            <i class="fa fa-shopping-cart">
              <span>{t("productDetails.cart")}</span>
            </i>
          </button>
        </>
      );
    } else {
      return (
        <>
          <h5 class="stok">
            {t("productDetails.stock")} {t("productDetails.outofstock")}
          </h5>
          <button className="add-minus" onClick={() => this.addStock()}>
            -
          </button>
          <input
            type="number"
            class="quantity"
            value={this.props.orderPerProduct}
            onChange={this.handleChange}
            name="jumlah"
          />
          <button className="add-minus" disabled>
            +
          </button>
          <button class="buy" onClick={this.zeroStock}>
            <i class="fa fa-shopping-cart">
              <span>{t("productDetails.cart")}</span>
            </i>
          </button>
        </>
      );
    }
  };

  zeroStock() {
    swal("Stok produk habis!", " ", "warning");
  }

  handleChange = (e) => {
    this.props.handleChange(e.target.value);
  };

  addToCart(e) {
    const totalHarga =
      parseInt(this.state.data.price) * parseInt(this.props.orderPerProduct);
    const product = {
      id: this.state.data.id,
      nama: this.state.data.nama,
      picture: this.state.data.picture,
      price: this.state.data.price,
      stock: this.state.data.stock,
      jumlah: this.props.orderPerProduct,
      totalHarga: totalHarga,
    };

    this.setState({ cart: product }, () => {
      console.log(this.state.cart);
      if (!this.props.orderPerProduct < 1) {
        this.props.wishList(this.state.cart);
        this.props.addOrderPerProduct(this.props.orderPerProduct);
      }
    });
    swal("Produk berhasil ditambah ke keranjang", " ", "success");
  }

  render() {
    const { data } = this.state;
    const { t } = this.props;
    const id = data.id;
    return (
      <div class="main-content">
        <div class="title mt-5 ml-5">
          <h4>{t("productDetails.title")}</h4>
        </div>
        <div class="row">
          <div class="col-sm-5">
            <div class="cart">
              <div class="card mt-3">
                <img
                  class="card-img-top"
                  src={data.picture}
                  alt="Card image cap"
                />
                <div class="card-body">
                  <h5 class="card-text">{data.nama}</h5>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <h3>{data.nama}</h3>
            <h3 class="prices">Rp. {data.price} / pcs</h3>
            {this.showStock()}

            <button
              className="see-other-product"
              onClick={() => this.props.setPage("Product")}
            >
              {t("productDetails.other")}
            </button>
            <button
              className="see-cart"
              onClick={() => this.props.setPage("Cart")}
            >
              {t("productDetails.view")}
            </button>
          </div>

          <div class="col-sm-4">
            <div class="card text-center details">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">
                      {t("productDetails.details.title")}
                    </a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <h5 class="card-title text-left">
                  {t("productDetails.details.sub-title1")}
                </h5>
                <p class="card-text text-left">
                  {t(`productDetails.details.product${id}.category`)}
                </p>
                <h5 class="card-title text-left mt-5">
                  {t("productDetails.details.sub-title2")}
                </h5>
                <p class="card-text text-left">
                  {t(`productDetails.details.product${id}.desc`)}
                </p>
                <h5 class="card-title text-left mt-5">
                  {t("productDetails.details.sub-title3")}
                </h5>
                <p class="card-text text-left">
                  {t(`productDetails.details.product${id}.dose`)}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    id: state.id,
    cart: state.cart,
    totalPrice: state.totalPrice,
    orderPerProduct: state.orderPerProduct,
    page: state.page,
    allCart: state.allCart,
    jumlahPerProduct: state.jumlahPerProduct,
  };
};

const mapDispatch = (dispatch) => {
  return {
    addCart: () => dispatch({ type: "ADD_CART" }),
    minusCart: () => dispatch({ type: "MINUS_CART" }),
    wishList: (val) => dispatch({ type: "ADD_TO_CART", value: val }),
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
    handleChange: (val) => dispatch({ type: "HANDLE_CHANGE", value: val }),
    addOrderPerProduct: (val) =>
      dispatch({ type: "GET_ORDER_PRODUCT", value: val }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatch
)(withTranslation()(DetailsProduct));

import { BrowserRouter, Route } from "react-router-dom";
import Login from "./page/Login/Login";
import Register from "./page/Login/Register";
import Routing from "./route/Routing";
import Chat from "./page/Chat/Chat";
import { useTranslation } from "react-i18next";
import { useState } from "react";

function App() {
  const { t, i18n } = useTranslation();
  const [language, setLanguage] = useState("id");

  function changeLanguage(e) {
    const code = e.target.value;

    if (i18n.language !== code) {
      i18n.changeLanguage(code);
      setLanguage(code);
    }
  }

  return (
    <>
      <BrowserRouter>
        <Route exact path="/home" component={Routing} />
        <Route exact path="/" component={Routing} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/" component={Chat} />
      </BrowserRouter>
      <div className="en">
        <button value="en" id="en" onClick={changeLanguage}>
          EN
        </button>
      </div>
      <div className = "id">
        <button value="id" id="id" onClick={changeLanguage}>
          ID
        </button>
      </div>
    </>
  );
}

export default App;

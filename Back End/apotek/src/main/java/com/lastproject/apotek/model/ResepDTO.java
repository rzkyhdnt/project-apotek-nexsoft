package com.lastproject.apotek.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResepDTO {
    private int id;
    private String date;
    private List<MultipartFile> resepImage;
    private String resepText;
    private String status = "Menunggu";
    private boolean deleted = false;
}

import React, { Component } from "react";
import icon from "../../assets/img/user.png";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/vendor/linearicons/style.css";
import { Dropdown } from "react-bootstrap";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

class NavbarMenu extends Component {
  handleClick = (e) => {
    this.props.sendToLogout();
    this.props.setPage(e);

    <Redirect to={"/home"} />
  };
  render() {
    return (
      <Dropdown>
        <Dropdown.Toggle variant="white" id="dropdown-basic">
          <img src={icon} />
          Admin
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item href="#/action-1">My Profile</Dropdown.Item>
          <Dropdown.Item onClick={() => this.handleClick("Home")}>Logout</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: state.isLogin,
    status: state.status,
  };
};

const mapToProps = (dispatch) => {
  return {
    sendToLogout: () => dispatch({ type: "LOGOUT_REQUEST" }),
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapToProps)(NavbarMenu);

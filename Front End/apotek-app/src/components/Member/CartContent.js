import React, { Component } from "react";
import Navbar from "./Navbar";
import HeaderInfo from "./HeaderInfo";
import CartContainer from "./Atomic/CartContainer";
import Footer from "./Atomic/Footer";
import { connect } from "react-redux";

class CartContent extends Component {
  render() {
    return (
      <>
        <HeaderInfo />
        <CartContainer />
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(CartContent);

import React, { Component } from "react";
import NavbarAdmin from "../../components/NavbarAdmin";
import SidebarAdmin from "../../components/SidebarAdmin";
import "bootstrap/dist/css/bootstrap.min.css";
import TableUser from "../../components/Atomic/TableUser";

export default class DataUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headers: [
        "Username",
        "Nama",
        "Alamat",
        "No. HP",
        "Email",
        "Tgl. Registrasi",
        "Status",
        "Actions",
      ],
    };
  }
  render() {
    const { headers } = this.state;
    return (
      <div id="wrapper">
        <NavbarAdmin />
        <SidebarAdmin />
        <TableUser title="Daftar Users" headers={headers} />
      </div>
    );
  }
}

package com.lastproject.apotek.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "obat")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Obat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotBlank
    private String nama;
    private int stock;
    private Long price;
    @NotBlank
    private String dosis;
    @NotBlank
    private String desc;
    @NotBlank
    private String jenis;
    @NotBlank
    private String picture;
    private boolean deleted = false;
}

import React, { Component } from "react";
import Cart from "../../../assets/img/Users/cart.png";
import { Badge } from "react-bootstrap";
import { connect } from "react-redux";

class CartIcon extends Component {
  render() {
    return (
      <>
        <button
          class="navbar-brand mr-5 mt-2"
          onClick={() => this.props.setPage("Cart")}
        >
          <a href="#">
            <img className="cart" src={Cart} width="30" height="30" alt="" />
            <Badge variant="danger">{this.props.totalOrder}</Badge>
          </a>
        </button>
      </>
    );
  }
  
}

const mapStateToProps = (state) => {
  return {
    totalOrder: state.totalOrder,
    page: state.page,
  };
};

const mapDispatch = (dispatch) => {
  return {
    setPage: (val) => dispatch({ type: "SET_PAGE", value: val }),
  };
};

export default connect(mapStateToProps, mapDispatch)(CartIcon);

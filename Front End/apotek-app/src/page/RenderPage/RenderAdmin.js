import React, { Component } from "react";
import { connect } from "react-redux";
import Dashboard from "../Admin/Dashboard";
import DataObat from "../Admin/DataObat";
import DataUsers from "../Admin/DataUsers";
import LaporanMember from "../Admin/LaporanMember";
import LaporanNonMember from "../Admin/LaporanNonMember";
import TambahAdmin from "../Admin/TambahAdmin";
import TambahObat from "../Admin/TambahObat";
import Resep from "../Admin/Resep";
import Chat from "../Admin/Chat";

export default function RenderAdmin(val) {
  switch (val) {
    case "":
      return <Dashboard />;
    case "Dashboard":
      return <Dashboard />;
    case "Daftar Obat":
      return <DataObat />;
    case "Daftar Users":
      return <DataUsers />;
    case "Laporan Member":
      return <LaporanMember />;
    case "Laporan Nonmember":
      return <LaporanNonMember />;
    case "Resep":
      return <Resep />;
    case "Tambah Admin":
      return <TambahAdmin />;
    case "Tambah Obat":
      return <TambahObat />;
    case "Chat Admin":
      return <Chat />;
  }
}

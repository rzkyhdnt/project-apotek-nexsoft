import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import rootReducer from "../reducer/rootReducer";

const persistConfig = {
  key: "root",
  storage,
  blacklist : ["orderPerProduct"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
let store = createStore(persistedReducer);
let persistor = persistStore(store);

export default () => {
  return {
    store,
    persistor,
  };
};
